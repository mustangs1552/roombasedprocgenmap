﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class RBMGAssetLibrary : AssetLibraryWithChances
    {
        [Tooltip("The rooms that are available to be used by the generator.")]
        [SerializeField] private List<RoomWithChance> rooms = new List<RoomWithChance>();
        [Tooltip("Entrances that are blocked or have no where to go.")]
        [SerializeField] private List<GameObjectWithChance> inaccessibleEntrances = new List<GameObjectWithChance>();
        [Tooltip("Entrances that are linked with another entrance.")]
        [SerializeField] private List<GameObjectWithChance> accessibleEntrances = new List<GameObjectWithChance>();
        [Tooltip("Entrances that are placed at the main entrances of new roomGroups.")]
        [SerializeField] private List<GameObjectWithChance> roomGroupEntrances = new List<GameObjectWithChance>();

        private List<ObjectWithChance<Room>> roomsObjsWithChance = null;
        private List<ObjectWithChance<GameObject>> inaccessibleEntrancesObjsWithChance = null;
        private List<ObjectWithChance<GameObject>> accessibleEntrancesObjsWithChance = null;
        private List<ObjectWithChance<GameObject>> roomGroupEntrancesObjsWithChance = null;

        #region Methods
        /// <summary>
        /// Gets a random room from the list of rooms using chances.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the objects not to choose.</param>
        /// <returns>The randomly chosen room.</returns>
        public Room GetRandomRoom(ref HashSet<int> invalidIndexes)
        {
            if (rooms == null || rooms.Count == 0) return null;
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();

            if (roomsObjsWithChance == null || roomsObjsWithChance.Count != rooms.Count) roomsObjsWithChance = RoomWithChance.ConvertRoomsWithChancesToObjectsWithChance(rooms);

            return ListUtility.GetRandomObjectWithChance(roomsObjsWithChance, ref invalidIndexes);
        }
        /// <summary>
        /// Gets a random inaccessible entrance from the list of inaccessible entrances using chances.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the objects not to choose.</param>
        /// <returns>The randomly chosen inaccessible entrance.</returns>
        public GameObject GetRandomInaccessibleEntrance(ref HashSet<int> invalidIndexes)
        {
            if (inaccessibleEntrances == null || inaccessibleEntrances.Count == 0) return null;
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();

            if (inaccessibleEntrancesObjsWithChance == null || inaccessibleEntrancesObjsWithChance.Count != inaccessibleEntrances.Count) inaccessibleEntrancesObjsWithChance = GameObjectWithChanceList.ConvertToObjectsWithChance(inaccessibleEntrances);

            return ListUtility.GetRandomObjectWithChance(inaccessibleEntrancesObjsWithChance, ref invalidIndexes);
        }
        /// <summary>
        /// Gets a random accessible entrance from the list of accessible entrances using chances.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the objects not to choose.</param>
        /// <returns>The randomly chosen accessible entrance.</returns>
        public GameObject GetRandomAccessibleEntrance(ref HashSet<int> invalidIndexes)
        {
            if (accessibleEntrances == null || accessibleEntrances.Count == 0) return null;
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();

            if (accessibleEntrancesObjsWithChance == null || accessibleEntrancesObjsWithChance.Count != accessibleEntrances.Count) accessibleEntrancesObjsWithChance = GameObjectWithChanceList.ConvertToObjectsWithChance(accessibleEntrances);

            return ListUtility.GetRandomObjectWithChance(accessibleEntrancesObjsWithChance, ref invalidIndexes);
        }
        /// <summary>
        /// Gets a random roomGroup entrance from the list of roomGroup entrances using chances.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the objects not to choose.</param>
        /// <returns>The randomly chosen roomGroup entrance.</returns>
        public GameObject GetRandomRoomGroupEntrance(ref HashSet<int> invalidIndexes)
        {
            if (roomGroupEntrances == null || roomGroupEntrances.Count == 0) return null;
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();

            if (roomGroupEntrancesObjsWithChance == null || roomGroupEntrancesObjsWithChance.Count != roomGroupEntrances.Count) roomGroupEntrancesObjsWithChance = GameObjectWithChanceList.ConvertToObjectsWithChance(roomGroupEntrances);

            return ListUtility.GetRandomObjectWithChance(roomGroupEntrancesObjsWithChance, ref invalidIndexes);
        }

        /// <summary>
        /// Clears the saved Room Generator related lists so that they can be repopulated.
        /// </summary>
        public void ClearRoomGeneratorGeneratedLists()
        {
            roomsObjsWithChance = null;
            inaccessibleEntrancesObjsWithChance = null;
            accessibleEntrancesObjsWithChance = null;
            roomGroupEntrancesObjsWithChance = null;
        }
        /// <summary>
        /// Clears the saved lists so that they can be repopulated.
        /// </summary>
        public void ClearAllGeneratedLists()
        {
            ClearGeneratedLists();
            ClearRoomGeneratorGeneratedLists();
        }
        #endregion

        [Serializable]
        private class RoomWithChance
        {
            public Room room = null;
            public int chance = 1;

            /// <summary>
            /// Converts to ObjectWithChance<Room>.
            /// </summary>
            /// <param name="roomsChances">The list to be converted.</param>
            /// <returns>The converted list.</returns>
            public ObjectWithChance<Room> ConvertToObjectWithChance()
            {
                return new ObjectWithChance<Room>()
                {
                    obj = room,
                    chance = this.chance
                };
            }

            /// <summary>
            /// Converts a List<RoomWithChance> into a List<ObjectWithChance<Room>>.
            /// </summary>
            /// <param name="roomsChances">The list to be converted.</param>
            /// <returns>The converted list.</returns>
            public static List<ObjectWithChance<Room>> ConvertRoomsWithChancesToObjectsWithChance(List<RoomWithChance> roomsChances)
            {
                if (roomsChances == null || roomsChances.Count == 0) return new List<ObjectWithChance<Room>>();

                List<ObjectWithChance<Room>> resutlingList = new List<ObjectWithChance<Room>>();
                foreach (RoomWithChance roomChance in roomsChances)
                {
                    resutlingList.Add(roomChance.ConvertToObjectWithChance());
                }

                return resutlingList;
            }
        }
    }
}
