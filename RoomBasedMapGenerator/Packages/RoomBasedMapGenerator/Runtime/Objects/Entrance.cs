﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.RoomBasedMapGenerator
{
    [RequireComponent(typeof(BoxCollider))]
    public class Entrance : MonoBehaviour
    {
        [Tooltip("The max distance away two entrances can be to be allowed to link up.")]
        [SerializeField] private float maxLinkDist = .5f;
        [Tooltip("Is debuging enabled?")]
        [SerializeField] private bool isDebug = false;

        /// <summary>
        /// The other entrance that this one is linked to.
        /// </summary>
        public Entrance OtherEntrance { get; set; }
        /// <summary>
        /// The room that this entrance belongs to.
        /// </summary>
        public Room Room { get; set; }
        /// <summary>
        /// The object that was placed in this entrance.
        /// </summary>
        public GameObject EntranceObject { get; set; }

        /// <summary>
        /// Is this entrance blocked by something.
        /// Setting this to false will also automatically set 'Couldn'tFitAnything' to false.
        /// </summary>
        public bool IsBlocked
        {
            get => isBlocked;
            set
            {
                isBlocked = value;
                if (!isBlocked) couldntFitAnything = false;
            }
        }
        /// <summary>
        /// This is a value that works with 'IsBlocked' and automatically sets it to true when this is set to true.
        /// This means that this entrance was marked blocked because the generator couldn't fit anything here (like a room or another generator).
        /// </summary>
        public bool CouldntFitAnything
        {
            get => couldntFitAnything;
            set
            {
                couldntFitAnything = value;
                if (couldntFitAnything) isBlocked = true;
            }
        }
        /// <summary>
        /// Is this an entrance to a new roomGroup?
        /// </summary>
        public bool IsRoomGroupEntrance { get; set; } = false;

        private BoxCollider obstructedDetection = null;
        private bool isBlocked = false;
        private bool couldntFitAnything = false;

        #region Methods
        /// <summary>
        /// Places the given object in the entrance.
        /// </summary>
        /// <param name="obj">The object to spawn.</param>
        /// <param name="EntranceSpawnedEvent">The event object to be called when the entrance places its entrance object.</param>
        public void PlaceEntranceObject(GameObject obj, UnityEvent<Entrance> EntranceSpawnedEvent = null)
        {
            if (!obj) return;

            DestroyEntranceObject();

            EntranceObject = ObjectPoolManager.SINGLETON.Instantiate(obj, transform.position, transform.rotation);
            if (OtherEntrance)
            {
                OtherEntrance.EntranceObject = EntranceObject;
                EntranceSpawnedEvent?.Invoke(this);
            }
        }
        /// <summary>
        /// Places an object in the entrance depending on if it goes anywhere or not.
        /// </summary>
        /// <param name="assetLib">The asset library with the objects to choose from.</param>
        /// <param name="EntranceSpawnedEvent">The event object to be called when the entrance places its entrance object.</param>
        public void PlaceEntranceObject(RBMGAssetLibrary assetLib, UnityEvent<Entrance> EntranceSpawnedEvent = null)
        {
            if (EntranceObject || !assetLib) return;

            HashSet<int> invalidIndexes = new HashSet<int>();
            GameObject chosenObj = null;
            if (!OtherEntrance || IsBlocked) chosenObj = assetLib.GetRandomInaccessibleEntrance(ref invalidIndexes)?.gameObject;
            else if (OtherEntrance)
            {
                if (IsRoomGroupEntrance)
                {
                    chosenObj = assetLib.GetRandomRoomGroupEntrance(ref invalidIndexes)?.gameObject;
                    if (chosenObj == null)
                    {
                        invalidIndexes = new HashSet<int>();
                        chosenObj = assetLib.GetRandomAccessibleEntrance(ref invalidIndexes)?.gameObject;
                    }
                }
                else chosenObj = assetLib.GetRandomAccessibleEntrance(ref invalidIndexes)?.gameObject;
            }

            EntranceObject = ObjectPoolManager.SINGLETON.Instantiate(chosenObj, transform.position, transform.rotation);
            if (OtherEntrance)
            {
                OtherEntrance.EntranceObject = EntranceObject;
                EntranceSpawnedEvent?.Invoke(this);
            }

            if (OtherEntrance && Vector3.Distance(transform.position, OtherEntrance.transform.position) > maxLinkDist) Debug.LogError($"{gameObject.name} of {Room.gameObject.name} linked with {OtherEntrance.gameObject.name} of {OtherEntrance.Room.gameObject.name} which should be too far ({Vector3.Distance(transform.position, OtherEntrance.transform.position)}/{maxLinkDist}) to be linked!\nIf this is a multi-floor room make sure each Entrance's local position is in relation the the parent Room object. The position of any parent objects between the Entrances and Room object must be zeroed out.\n");
        }
        /// <summary>
        /// Destroy the object that was spawned in this entrance.
        /// </summary>
        public void DestroyEntranceObject()
        {
            if (EntranceObject)
            {
                ObjectPoolManager.SINGLETON.Destroy(EntranceObject);
                EntranceObject = null;
                if (OtherEntrance) OtherEntrance.EntranceObject = null;
            }
        }

        /// <summary>
        /// Check if this entrance is obstructed by anything or is within range of another entrance to link up with.
        /// </summary>
        public void CheckEntranceObstruction()
        {
            if (OtherEntrance != null) return;

            Vector3 obstructionCenter = transform.position + obstructedDetection.center;
            Collider[] colliders = Physics.OverlapBox(obstructionCenter, obstructedDetection.size / 2, transform.rotation);
            List<Collider> entranceColliders = new List<Collider>();
            List<Collider> otherColliders = new List<Collider>();
            foreach (Collider col in colliders)
            {
                if (col.gameObject == gameObject) continue;
                else if (col.GetComponent<Entrance>() != null) entranceColliders.Add(col);
                else otherColliders.Add(col);
            }

            if (entranceColliders.Count > 0 && Vector3.Distance(transform.position, entranceColliders[0].transform.position) <= maxLinkDist)
            {
                Entrance otherEntrance = entranceColliders[0].GetComponent<Entrance>();
                if (otherEntrance != null && otherEntrance.Room != Room && otherEntrance.OtherEntrance == null && !otherEntrance.IsBlocked)
                {
                    otherEntrance.OtherEntrance = this;
                    OtherEntrance = otherEntrance;
                }
            }
            else if (otherColliders.Count > 0) IsBlocked = true;
        }
        #endregion

        private void Awake()
        {
            obstructedDetection = GetComponent<BoxCollider>();
        }

        private void OnDrawGizmos()
        {
            if (isDebug)
            {
                if (IsBlocked)
                {
                    Gizmos.color = (CouldntFitAnything) ? Color.magenta : Color.red;
                    Gizmos.DrawSphere(transform.position + (transform.forward * -.5f), .5f);
                }
                else if (OtherEntrance != null)
                {
                    Gizmos.color = (IsRoomGroupEntrance) ? Color.blue : Color.green;
                    Gizmos.DrawSphere(transform.position + (transform.forward * -.5f), .5f);

                    // If a line between two entrances are visible then that is bad, they aren't lined up properly.
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(transform.position, OtherEntrance.transform.position);
                }
                else
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawSphere(transform.position + (transform.forward * -.5f), .5f);
                }
            }
        }

        private void OnValidate()
        {
            if (maxLinkDist < 0) maxLinkDist = 0;
        }
    }
}
