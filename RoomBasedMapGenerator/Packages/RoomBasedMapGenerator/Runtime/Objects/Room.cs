﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class Room : MonoBehaviour
    {
        [Tooltip("The entrances that this room has.")]
        [SerializeField] private List<Entrance> entrances = new List<Entrance>();
        [Tooltip("Layer with the placement colliders for collision detection when placing rooms.")]
        [SerializeField] private string placementColliderLayer = "GeneratorRoomCollider";
        [Tooltip("The colliders that will be used to draw overlap cubes to detect collisions when placing room.")]
        [SerializeField] private List<BoxCollider> placementColliders = new List<BoxCollider>();
        [Tooltip("The object spawn nodes that are in this room.")]
        [SerializeField] private List<SpawnObjectNodeViaAssetLibrary> objSpawnNodes = new List<SpawnObjectNodeViaAssetLibrary>();
        [Tooltip("Is debuging enabled?")]
        [SerializeField] private bool isDebug = false;

        /// <summary>
        /// The entrances that this room has.
        /// </summary>
        public List<Entrance> Entrances => entrances;
        /// <summary>
        /// The entrances that this room has that are not linked to any other entrance and are not blcoked by something.
        /// </summary>
        public List<Entrance> UnlinkedUnblockedEntrances
        {
            get
            {
                List<Entrance> unlinkedEntrances = new List<Entrance>();
                foreach (Entrance entrance in entrances)
                {
                    if (entrance.OtherEntrance == null && !entrance.IsBlocked) unlinkedEntrances.Add(entrance);
                }

                return unlinkedEntrances;
            }
        }
        /// <summary>
        /// The entrances that this room has that are linked to another entrance.
        /// </summary>
        public List<Entrance> LinkedEntrances
        {
            get
            {
                List<Entrance> linkedEntrances = new List<Entrance>();
                foreach (Entrance entrance in entrances)
                {
                    if (entrance.OtherEntrance != null) linkedEntrances.Add(entrance);
                }

                return linkedEntrances;
            }
        }

        /// <summary>
        /// The Procedual Room Generator that generated this room.
        /// </summary>
        public RandomRoomsGenerator Generator { get; set; }

        /// <summary>
        /// The object spawn nodes that are in this room.
        /// </summary>
        public List<SpawnObjectNodeViaAssetLibrary> ObjSpawnNodes => objSpawnNodes;

        #region Methods
        /// <summary>
        /// Attempt to place this room at the given entrance.
        /// </summary>
        /// <param name="otherEntrance">The entrance that this room would need to be placed at.</param>
        /// <param name="canLinkWithOtherGenerators">Can this room link with other rooms from other generators?</param>
        /// <param name="generator">The generator placing this room.</param>
        /// <returns>True if successful.</returns>
        public bool AttemptPlacement(Entrance otherEntrance, bool canLinkWithOtherGenerators = true, RandomRoomsGenerator generator = null)
        {
            List<Entrance> unlinkedEntrances = UnlinkedUnblockedEntrances;
            if (unlinkedEntrances == null || unlinkedEntrances.Count == 0) return false;

            HashSet<int> failedEntrances = new HashSet<int>();
            Entrance selectedEntrance = null;
            bool invalidPlacement = true;
            while (invalidPlacement)
            {
                if (generator && generator.GenerationTimeUp) break;

                selectedEntrance = ListUtility.GetValidRandomObject(unlinkedEntrances, ref failedEntrances);
                if (selectedEntrance == null) return false;
                MoveToEntrance(otherEntrance, selectedEntrance);

                invalidPlacement = CheckCollision();
            }

            if (selectedEntrance != null)
            {
                otherEntrance.OtherEntrance = selectedEntrance;
                selectedEntrance.OtherEntrance = otherEntrance;

                foreach (Entrance entrance in unlinkedEntrances) entrance.CheckEntranceObstruction();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Places objects in entrances depending on if they go anywhere or not.
        /// </summary>
        /// <param name="assetLib">The asset library with the objects to choose from.</param>
        /// <param name="EntranceSpawnedEvent">The event object to be called when the entrances place thier entrance objects.</param>
        public void PlaceEntranceObjects(RBMGAssetLibrary assetLib, UnityEvent<Entrance> EntranceSpawnedEvent)
        {
            foreach (Entrance entrance in entrances) entrance.PlaceEntranceObject(assetLib, EntranceSpawnedEvent);
        }

        #region Private
        /// <summary>
        /// Checks the collision using the placment colliders to draw overlap cubes to detect other colliders.
        /// </summary>
        /// <returns>True if a colision is detected.</returns>
        private bool CheckCollision()
        {
            List<Collider> detectedObjs = new List<Collider>();
            Collider[] colliders = null;
            BoxCollider boxCol = null;
            foreach (BoxCollider placementCol in placementColliders)
            {
                colliders = Physics.OverlapBox(placementCol.transform.position, placementCol.size / 2, placementCol.transform.rotation, (int)LayerMask.GetMask(placementColliderLayer));
                foreach (Collider col in colliders)
                {
                    boxCol = col.GetComponent<BoxCollider>();
                    if (col.gameObject == gameObject || (boxCol && placementColliders.Contains(boxCol))) continue;
                    else
                    {
                        detectedObjs.Add(col);
                        if(isDebug) Debug.DrawLine(placementCol.transform.position, col.transform.position);
                    }
                }
            }

            return detectedObjs.Count > 0;
        }

        /// <summary>
        /// Move the room so that the given entrance that belongs to this room lines up with the other entrance that it should be linked to.
        /// </summary>
        /// <param name="otherEntrance">The other entrance that our own entrance should be linked to.</param>
        /// <param name="ownEntrance">Our entrance that should be linked to the other entrance.</param>
        private void MoveToEntrance(Entrance otherEntrance, Entrance ownEntrance)
        {
            transform.position = otherEntrance.transform.position;
            transform.Translate(-ownEntrance.transform.localPosition);

            float angle = UnityMathUtility.FindAngleXZ(otherEntrance.transform, ownEntrance.transform, Vector3.forward);
            transform.RotateAround(ownEntrance.transform.position, Vector3.up, -(angle - 180));
        }

        /// <summary>
        /// Check to see if all required values are set.
        /// </summary>
        private void CheckForMissingRequiredValues()
        {
            if (entrances == null || entrances.Count == 0) UnityLoggingUtility.LogMissingValue(GetType(), "entrances", gameObject);
            if (placementColliders == null || placementColliders.Count == 0) UnityLoggingUtility.LogMissingValue(GetType(), "placementColliders", gameObject);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (entrances == null || entrances.Count == 0) Debug.LogError("No 'entrances' set!");
            else entrances.ForEach(x => x.Room = this);
        }
        #endregion
        #endregion

        private void Awake()
        {
            CheckForMissingRequiredValues();
            Setup();
        }

        private void OnDrawGizmos()
        {
            if (isDebug)
            {
                if (CheckCollision()) Gizmos.color = Color.red;
                else Gizmos.color = Color.green;
                foreach (BoxCollider col in placementColliders) Gizmos.DrawCube(col.transform.position, col.size / 2);
            }
        }
    }
}
