﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class RandomRoomGroupsGenerator : MonoBehaviour
    {
        private enum RandomRoomGroupsGeneratorModes
        {
            Random,
            Sequential,
            Mixed
        }

        [Tooltip("The total number of roomGroups to generate.")]
        [SerializeField] private int maxRoomGroups = 3;
        [Tooltip("The minimum amount of clearence needed when choosing an entrance to start a new roomGroup.")]
        [SerializeField] private Vector3 minClearanceRequired = new Vector3(50, 50, 50);
        [Tooltip("The distance in front of the current entrance when checking for clearance for a new roomGroup.")]
        [SerializeField] private float clearanceAreaDist = .1f;
        [Tooltip("The starting entrance for the whole system.")]
        [SerializeField] private Entrance startingEntrance = null;
        [Tooltip("The mode that determines how the generator picks room templates to spawn.")]
        [SerializeField] private RandomRoomGroupsGeneratorModes roomGroupsGeneratorMode = RandomRoomGroupsGeneratorModes.Random;
        [Tooltip("(For Mixed Mode) The number of generator templates to spawn sequentially before swicthing to a random selection.")]
        [SerializeField] private int sequentialRoomGroupsBeforeRandom = 1;
        [Tooltip("The templates to use for the RandomRoomsGenerator settings that the roomGroups will use.")]
        [SerializeField] private List<RandomRoomsGenerator> roomGroupTemplates = new List<RandomRoomsGenerator>();
        [Tooltip("Start each new group at the farthest available entrance from the start entrance or randomly?")]
        [SerializeField] private bool startNewGroupAtFarthestEntrance = false;
        [Tooltip("Max time allowed for generating map before it is canceled.")]
        [SerializeField] private float maxGenerationTime = 10;
        [Tooltip("Auto generate map on start?")]
        [SerializeField] private bool autoGenerate = false;
        [Tooltip("Is debuging enabled?")]
        [SerializeField] private bool isDebug = false;

        [SerializeField] private UnityEvent OnGeneratorFinished = null;

        /// <summary>
        /// The starting entrance of this generator.
        /// </summary>
        public Entrance StartingEntrance => startingEntrance;

        /// <summary>
        /// The generators that were spawned by this groups generator.
        /// </summary>
        public List<RandomRoomsGenerator> Generators => (generatedRoomsGenerators != null) ? generatedRoomsGenerators : new List<RandomRoomsGenerator>();

        /// <summary>
        /// The entrances that all the rooms of all the roomGroups have that are not linked to any other entrance.
        /// </summary>
        public List<Entrance> UnlinkedUnblockedEntrances
        {
            get
            {
                if (generatedRoomsGenerators == null || generatedRoomsGenerators.Count == 0) return new List<Entrance>();

                List<Entrance> entrances = new List<Entrance>();
                foreach (RandomRoomsGenerator generator in generatedRoomsGenerators)
                {
                    foreach(Entrance entrance in generator.UnlinkedUnblockedEntrances)
                    {
                        if (!entrances.Contains(entrance)) entrances.Add(entrance);
                    }
                }
                return entrances;
            }
        }

        /// <summary>
        /// The object spawn nodes that are in the rooms of all the generated generators including the starting room.
        /// </summary>
        public List<SpawnObjectNodeViaAssetLibrary> ObjSpawnNodes
        {
            get
            {
                List<SpawnObjectNodeViaAssetLibrary> nodes = new List<SpawnObjectNodeViaAssetLibrary>();
                Generators.ForEach(x => x.ObjSpawnNodes.ForEach(y => nodes.Add(y)));

                return nodes;
            }
        }
        /// <summary>
        /// The object spawn nodes that are in the rooms of all the generated generators not including the starting room.
        /// </summary>
        public List<SpawnObjectNodeViaAssetLibrary> ObjSpawnNodesWithoutStartRoom
        {
            get
            {
                List<SpawnObjectNodeViaAssetLibrary> nodes = new List<SpawnObjectNodeViaAssetLibrary>();
                Generators.ForEach(x => x.ObjSpawnNodes.ForEach(y => nodes.Add(y)));

                return nodes;
            }
        }

        /// <summary>
        /// Is the generator running?
        /// </summary>
        public bool IsRunning => isRunning;
        /// <summary>
        /// Is the generator's max time up?
        /// </summary>
        public bool GenerationTimeUp => maxGenerationTime > 0 && (DateTime.Now - startTime).TotalSeconds > maxGenerationTime;
        /// <summary>
        /// The last time this generator ran it was canceled early for some reason.
        /// </summary>
        public bool CanceledGeneration => canceledGeneration;

        private List<RandomRoomsGenerator> generatedRoomsGenerators = new List<RandomRoomsGenerator>();
        private DateTime startTime = new DateTime();
        private Transform roomGroupAnchor = null;
        private bool isRunning = false;
        private bool passActive = false;
        Entrance currEntrance = null;
        HashSet<int> invalidEntranceIndexes = new HashSet<int>();
        RandomRoomsGenerator generatedGenerator = null;
        private int chosenRoomGroup = 0;
        private Coroutine currRoomsGeneratorCoroutine = null;
        private bool autoStarted = false;
        private bool canceledGeneration = false;

        #region Methods
        /// <summary>
        /// Generate the map of roomGroups.
        /// </summary>
        public void GenerateRoomGroups()
        {
            if (maxRoomGroups <= 0 || startingEntrance == null || roomGroupTemplates == null || roomGroupTemplates.Count == 0 || minClearanceRequired == null) return;
            canceledGeneration = false;
            isRunning = true;
            startTime = DateTime.Now;
            chosenRoomGroup = 0;

            DestroyRoomGroups();
            StartCoroutine(CoroutineUtility.WhileCoroutine(GeneratorPass, FinishGenerating));
        }

        /// <summary>
        /// Destroy the generated map of roomGroups.
        /// </summary>
        public void DestroyRoomGroups()
        {
            foreach (RandomRoomsGenerator generator in generatedRoomsGenerators)
            {
                generator.DestroyRooms();
                GameObject.Destroy(generator);
            }

            generatedRoomsGenerators = new List<RandomRoomsGenerator>();
        }

        /// <summary>
        /// Send this generator object to the given gameobject.
        /// </summary>
        /// <param name="obj">The gameobject to receive this generator.</param>
        public virtual void SendGeneratorToObject(GameObject obj)
        {
            if (!obj) return;
            IGeneratorRelatedObject[] genSpawnedObjs = obj.GetComponentsInChildren<IGeneratorRelatedObject>();
            if (genSpawnedObjs == null || genSpawnedObjs.Length == 0) return;

            foreach (IGeneratorRelatedObject spawnedObj in genSpawnedObjs) spawnedObj.ReceiveRandomRoomGroupsGenerator(this);
        }

        #region Private
        /// <summary>
        /// Checks an area in front of the given position to see if the area is clear of other colliders.
        /// </summary>
        /// <param name="trans"></param>
        /// <returns>True if the area is clear of other colliders.</returns>
        private bool CheckForClearence(Transform trans)
        {
            Vector3 clearenceAreaCenter = trans.position + trans.forward * (clearanceAreaDist + (minClearanceRequired.z / 2));
            Collider[] detectedCols = Physics.OverlapBox(clearenceAreaCenter, minClearanceRequired / 2, trans.rotation);
            bool isBlocked = false;
            foreach (Collider col in detectedCols)
            {
                if (col.GetComponent<Entrance>() == null)
                {
                    isBlocked = true;
                    break;
                }
            }

            return !isBlocked;
        }

        /// <summary>
        /// Check if a new generator can be started and start a generator pass if it can.
        /// </summary>
        /// <returns>True if successful.</returns>
        private bool StartGeneratorPass()
        {
            if (passActive) return true;

            currEntrance = null;
            invalidEntranceIndexes = new HashSet<int>();
            generatedGenerator = null;

            if (generatedRoomsGenerators.Count == 0) currEntrance = startingEntrance;
            else
            {
                if (!startNewGroupAtFarthestEntrance) currEntrance = ListUtility.GetValidRandomObject(UnlinkedUnblockedEntrances, ref invalidEntranceIndexes);
                else currEntrance = RBMGDistanceUtility.FindFarthestEntranceByRoomAStar((startingEntrance.Room) ? startingEntrance.Room : generatedRoomsGenerators[0].StartingRoom, UnlinkedUnblockedEntrances);
            }

            if (currEntrance == null) return false;
            if (!CheckForClearence(currEntrance.transform))
            {
                currEntrance.CouldntFitAnything = true;
                return true;
            }

            if (roomGroupsGeneratorMode == RandomRoomGroupsGeneratorModes.Random || (roomGroupsGeneratorMode == RandomRoomGroupsGeneratorModes.Mixed && generatedRoomsGenerators.Count > sequentialRoomGroupsBeforeRandom)) chosenRoomGroup = UnityEngine.Random.Range(0, roomGroupTemplates.Count);
            if (roomGroupAnchor == null)
            {
                roomGroupAnchor = new GameObject("ZONES_ANCHOR").transform;
                roomGroupAnchor.parent = transform;
            }

            generatedGenerator = ObjectPoolManager.SINGLETON.Instantiate(roomGroupTemplates[chosenRoomGroup].gameObject, true, false).GetComponent<RandomRoomsGenerator>();
            generatedGenerator.gameObject.name = $"{roomGroupTemplates[chosenRoomGroup].name}-{generatedRoomsGenerators.Count}";
            generatedGenerator.transform.parent = roomGroupAnchor;
            generatedGenerator.GenerateRooms(false, currEntrance);
            passActive = true;
            return true;
        }
        /// <summary>
        /// Finalize the current pass and prepare for the next one.
        /// </summary>
        private void FinishGeneratorPass()
        {
            if (generatedGenerator)
            {
                if (generatedGenerator.GeneratedRooms.Count == 0) ObjectPoolManager.SINGLETON.Destroy(generatedGenerator.gameObject);
                else
                {
                    generatedRoomsGenerators.Add(generatedGenerator);
                    invalidEntranceIndexes = new HashSet<int>();
                }
            }

            if (roomGroupsGeneratorMode == RandomRoomGroupsGeneratorModes.Sequential || (roomGroupsGeneratorMode == RandomRoomGroupsGeneratorModes.Mixed && generatedRoomsGenerators.Count <= sequentialRoomGroupsBeforeRandom))
            {
                chosenRoomGroup++;
                if (chosenRoomGroup >= roomGroupTemplates.Count) chosenRoomGroup = 0;
            }

            passActive = false;
        }
        /// <summary>
        /// Main method for a room generator pass.
        /// Each pass is generating one RandomRoomGenerator.
        /// </summary>
        /// <returns>True if generator is finished and no other pass is needed.</returns>
        private bool GeneratorPass()
        {
            if (generatedRoomsGenerators.Count >= maxRoomGroups) return false;
            if (GenerationTimeUp)
            {
                Debug.LogError("Canceling groups generation due to max time reached!");
                if (currRoomsGeneratorCoroutine != null) StopCoroutine(currRoomsGeneratorCoroutine);
                canceledGeneration = true;
                return false;
            }

            if (!passActive)
            {
                if (!StartGeneratorPass()) return false;
                currRoomsGeneratorCoroutine = StartCoroutine(CoroutineUtility.WhileCoroutine(CheckIsGeneratorRunning, FinishGeneratorPass));
            }
            return true;
        }
        /// <summary>
        /// Check if the current RandomRoomGenerator is running.
        /// </summary>
        /// <returns>True if it is running.</returns>
        private bool CheckIsGeneratorRunning()
        {
            if (!generatedGenerator) return false;

            return generatedGenerator.IsRunning;
        }

        /// <summary>
        /// Finalize the generation of this groups generator.
        /// </summary>
        private void FinishGenerating()
        {
            foreach (RandomRoomsGenerator generator in generatedRoomsGenerators)
            {
                foreach (Entrance entrance in generator.UsedStartingRoomEntrances)
                {
                    entrance.IsRoomGroupEntrance = true;
                    entrance.OtherEntrance.IsRoomGroupEntrance = true;
                }
            }
            generatedRoomsGenerators.ForEach(x => x.PlaceEntranceObjects());

            Debug.Log($"<color=lime>Generated {generatedRoomsGenerators.Count} room group(s) in {(DateTime.Now - startTime).TotalSeconds.ToString("F3")} second(s).</color>");
            isRunning = false;
            OnGeneratorFinished?.Invoke();
        }

        /// <summary>
        /// Check to see if all required values are set.
        /// </summary>
        private void CheckForMissingRequiredValues()
        {
            if (!startingEntrance) UnityLoggingUtility.LogMissingValue(GetType(), "startingEntrance", gameObject);
            if (roomGroupTemplates == null || roomGroupTemplates.Count == 0) UnityLoggingUtility.LogMissingValue(GetType(), "roomGroupEntrance", gameObject);
        }
        #endregion
        #endregion

        private void Awake()
        {
            CheckForMissingRequiredValues();
        }

        private void FixedUpdate()
        {
            if (!autoStarted && autoGenerate)
            {
                autoStarted = true;
                GenerateRoomGroups();
            }
        }

        private void OnDrawGizmos()
        {
            if (isDebug)
            {
                if (startingEntrance != null)
                {
                    Gizmos.color = (CheckForClearence(startingEntrance.transform)) ? Color.green : Color.red;
                    Vector3 clearenceAreaCenter = startingEntrance.transform.position + startingEntrance.transform.forward * (clearanceAreaDist + (minClearanceRequired.z / 2));
                    Gizmos.DrawWireCube(clearenceAreaCenter, minClearanceRequired);
                }
            }
        }

        private void OnValidate()
        {
            if (maxRoomGroups < 0) maxRoomGroups = 0;
            if (minClearanceRequired.x < 0) minClearanceRequired.x = 0;
            if (minClearanceRequired.y < 0) minClearanceRequired.y = 0;
            if (minClearanceRequired.z < 0) minClearanceRequired.z = 0;
            if (clearanceAreaDist < 0) clearanceAreaDist = 0;
            if (sequentialRoomGroupsBeforeRandom < 0) sequentialRoomGroupsBeforeRandom = 0;
            if (maxGenerationTime < 0) maxGenerationTime = 0;
        }
    }
}
