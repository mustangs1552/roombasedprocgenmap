﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class RandomRoomsGenerator : MonoBehaviour
    {
        [Tooltip("The max amount of rooms that could be spawned.")]
        [SerializeField] private int maxRooms = 10;
        [Tooltip("The entrance to start placing rooms at.")]
        [SerializeField] private Entrance startingEntrance = null;
        [Tooltip("Only use one entrance from the start room (startingEntrance) to spawn rooms or allow to use more from the starting room if available?")]
        [SerializeField] private bool onlyUseOneStartRoomEntrance = false;
        [Tooltip("Can rooms link up with another from a different generator?")]
        [SerializeField] private bool canLinkWithOtherGenerators = true;
        [Tooltip("The library of assets to be used by this generator.")]
        [SerializeField] private RBMGAssetLibrary assetLib = null;
        [Tooltip("Max time allowed for generating map before it is canceled.")]
        [SerializeField] private float maxGenerationTime = 10;
        [Tooltip("The time waited between spawning each room. Increasing this can improve collision detection when placing rooms.")]
        [SerializeField] private float roomSpawnTimeGap = .25f;
        [Tooltip("Auto generate map on start?")]
        [SerializeField] private bool autoGenerate = false;
        [Tooltip("Is debuging enabled?")]
        [SerializeField] private bool isDebug = false;

        [Tooltip("Called when the generator has finished spawning all the rooms and entrances.")]
        public UnityEvent OnGeneratorFinished = new UnityEvent();
        [Tooltip("Called when a room was successfully spawned. Passes the spawned Room.")]
        public RoomUnityEvent OnRoomSpawned = new RoomUnityEvent();
        [Tooltip("Called when an entrance object was successfully spawned. Passes the Entrance in question.")]
        public EntranceUnityEvent OnEntranceSpawned = new EntranceUnityEvent();

        /// <summary>
        /// All the rooms that have been generated.
        /// </summary>
        public List<Room> GeneratedRooms => generatedRooms;
        /// <summary>
        /// The generator's starting Room.
        /// </summary>
        public Room StartingRoom => startingRoom;
        /// <summary>
        /// The generator's starting entrance.
        /// </summary>
        public Entrance StartingEntrance => startingEntrance;

        /// <summary>
        /// The entrances that all the rooms have that are not linked to any other entrance.
        /// </summary>
        public List<Entrance> UnlinkedUnblockedEntrances
        {
            get
            {
                List<Entrance> unlinkedEntrances = new List<Entrance>();
                if (startingRoom != null && !onlyUseOneStartRoomEntrance) startingRoom.UnlinkedUnblockedEntrances.ForEach(x => unlinkedEntrances.Add(x));
                foreach (Room room in generatedRooms) room.UnlinkedUnblockedEntrances.ForEach(x => unlinkedEntrances.Add(x));
                return unlinkedEntrances;
            }
        }
        /// <summary>
        /// All the entrances that this generator used from the starting room.
        /// </summary>
        public List<Entrance> UsedStartingRoomEntrances
        {
            get
            {
                List<Entrance> startingEntrances = new List<Entrance>();
                startingEntrances.Add(startingEntrance);
                if (startingRoom != null)
                {
                    foreach (Entrance entrance in startingRoom.LinkedEntrances)
                    {
                        if (entrance != startingEntrance && generatedRooms.Contains(entrance.OtherEntrance.Room)) startingEntrances.Add(entrance);
                    }
                }

                return startingEntrances;
            }
        }
        /// <summary>
        /// All the entrances that this generator has that are linked to another entrance.
        /// </summary>
        public List<Entrance> LinkedEntrances
        {
            get
            {
                List<Entrance> entrances = new List<Entrance>();
                if (startingRoom != null) startingRoom.LinkedEntrances.ForEach(x => entrances.Add(x));
                foreach (Room room in generatedRooms) room.LinkedEntrances.ForEach(x => entrances.Add(x));
                return entrances;
            }
        }

        /// <summary>
        /// The object spawn nodes that are in the rooms of this generator including the starting room.
        /// </summary>
        public List<SpawnObjectNodeViaAssetLibrary> ObjSpawnNodes
        {
            get
            {
                List<SpawnObjectNodeViaAssetLibrary> nodes = new List<SpawnObjectNodeViaAssetLibrary>();
                if (startingRoom != null) startingRoom.ObjSpawnNodes.ForEach(x => nodes.Add(x));
                GeneratedRooms.ForEach(x => x.ObjSpawnNodes.ForEach(y => nodes.Add(y)));

                return nodes;
            }
        }
        /// <summary>
        /// The object spawn nodes that are in the rooms of this generator not including the starting room.
        /// </summary>
        public List<SpawnObjectNodeViaAssetLibrary> ObjSpawnNodesWithoutStartRoom
        {
            get
            {
                List<SpawnObjectNodeViaAssetLibrary> nodes = new List<SpawnObjectNodeViaAssetLibrary>();
                GeneratedRooms.ForEach(x => x.ObjSpawnNodes.ForEach(y => nodes.Add(y)));

                return nodes;
            }
        }

        /// <summary>
        /// Is the generator running?
        /// </summary>
        public bool IsRunning => isRunning;
        /// <summary>
        /// Is the generator's max time up?
        /// </summary>
        public bool GenerationTimeUp => maxGenerationTime > 0 && (DateTime.Now - startTime).TotalSeconds > maxGenerationTime;
        /// <summary>
        /// The last time this generator ran it was canceled early for some reason.
        /// </summary>
        public bool CanceledGeneration => canceledGeneration;

        private List<Room> generatedRooms = new List<Room>();
        private Room startingRoom = null;
        private DateTime startTime = new DateTime();
        private bool isRunning = false;
        private Transform roomsAnchor = null;
        private bool currAutoPlaceEntranceObjects = false;
        private bool autoStarted = false;
        private bool canceledGeneration = false;

        #region Methods
        /// <summary>
        /// Generate the map of rooms.
        /// </summary>
        /// <param name="autoPlaceEntranceObjects">Place the entrance objects once room spawning is done?</param>
        /// <param name="customStartingEntrance">Overwrite the default starting entrance with this one and use it.</param>
        public void GenerateRooms(bool autoPlaceEntranceObjects = true, Entrance customStartingEntrance = null)
        {
            if (customStartingEntrance != null) startingEntrance = customStartingEntrance;

            if (!startingEntrance)
            {
                UnityLoggingUtility.LogMissingValue(GetType(), "startingEntrance", gameObject);
                return;
            }
            if (maxRooms <= 0 || assetLib == null) return;
            canceledGeneration = false;
            isRunning = true;
            startTime = DateTime.Now;

            DestroyRooms();
            startingRoom = (startingEntrance.Room != null) ? startingEntrance.Room : null;

            if (!PlaceRoomRandomly(startingEntrance))
            {
                Debug.LogError("Unable to place first room!");
                isRunning = false;
                canceledGeneration = true;
                return;
            }
            startingEntrance.IsRoomGroupEntrance = true;
            startingEntrance.OtherEntrance.IsRoomGroupEntrance = true;

            currAutoPlaceEntranceObjects = autoPlaceEntranceObjects;
            StartCoroutine(DelayedStartGeneratorPasses(roomSpawnTimeGap));
        }

        /// <summary>
        /// Destroy the generated map of rooms.
        /// </summary>
        public void DestroyRooms()
        {
            foreach (Room room in generatedRooms)
            {
                room.Entrances.ForEach(x => x.DestroyEntranceObject());
                room.ObjSpawnNodes.ForEach(x => x.DestroyLastSpawnedObject());
                ObjectPoolManager.SINGLETON.Destroy(room.gameObject);
            }
            generatedRooms = new List<Room>();
        }

        /// <summary>
        /// Places objects in entrances depending on if they go anywhere or not.
        /// </summary>
        public void PlaceEntranceObjects()
        {
            ApplyEntranceStates();

            if (startingRoom) startingRoom.PlaceEntranceObjects(assetLib, OnEntranceSpawned);
            foreach (Room room in generatedRooms) room.PlaceEntranceObjects(assetLib, OnEntranceSpawned);
        }
        /// <summary>
        /// Apply the entrance states according to this generator's options.
        /// </summary>
        public void ApplyEntranceStates()
        {
            ApplyEntranceStatesForStartingRoom();
            ApplyEntranceStatesForOtherGeneratorLinks();
        }

        /// <summary>
        /// Send this generator object to the given gameobject.
        /// </summary>
        /// <param name="obj">The gameobject to receive this generator.</param>
        public virtual void SendGeneratorToObject(GameObject obj)
        {
            if (!obj) return;
            IGeneratorRelatedObject[] genSpawnedObjs = obj.GetComponentsInChildren<IGeneratorRelatedObject>();
            if (genSpawnedObjs == null || genSpawnedObjs.Length == 0) return;

            foreach (IGeneratorRelatedObject spawnedObj in genSpawnedObjs) spawnedObj.ReceiveRandomRoomsGenerator(this);
        }

        #region Private
        /// <summary>
        /// Starts the generator passes with an initial delay.
        /// </summary>
        /// <param name="delay">Time to wait before starting the generator passes.</param>
        private IEnumerator DelayedStartGeneratorPasses(float delay)
        {
            yield return new WaitForSeconds(delay);
            StartCoroutine(CoroutineUtility.WhileCoroutine(GeneratorPass, FinishGeneration, roomSpawnTimeGap));
        }
        /// <summary>
        /// One generator pass which attempts to spawn a room.
        /// </summary>
        /// <returns>True if it can still continue.</returns>
        private bool GeneratorPass()
        {
            if (generatedRooms.Count >= maxRooms) return false;
            if (GenerationTimeUp)
            {
                Debug.LogError("Canceling rooms generation due to max time reached!");
                canceledGeneration = true;
                return false;
            }

            if (!PlaceRoomRandomly()) return false;

            OnRoomSpawned?.Invoke(generatedRooms[generatedRooms.Count - 1]);
            return true;
        }
        /// <summary>
        /// Finalize the map generation.
        /// </summary>
        private void FinishGeneration()
        {
            if (currAutoPlaceEntranceObjects) PlaceEntranceObjects();

            Debug.Log($"<color=green>Generated {generatedRooms.Count} room(s) in {(DateTime.Now - startTime).TotalSeconds.ToString("F3")} second(s).</color>");
            isRunning = false;
            OnGeneratorFinished?.Invoke();
        }

        /// <summary>
        /// Places a room randomly by attempting to place a room at each entrance and then giving up if no room can be placed at any.
        /// </summary>
        /// <returns>True if successful.</returns>
        private bool PlaceRoomRandomly()
        {
            List<Entrance> unlinkedEntrances = UnlinkedUnblockedEntrances;
            if (unlinkedEntrances == null || unlinkedEntrances.Count == 0) return false;

            Entrance selectedEntrance = null;
            HashSet<int> failedRandEntrances = new HashSet<int>();
            bool roomGenerated = false;
            while (!roomGenerated && !GenerationTimeUp)
            {
                selectedEntrance = ListUtility.GetValidRandomObject(unlinkedEntrances, ref failedRandEntrances);
                if (selectedEntrance == null) return false;

                roomGenerated = PlaceRoomRandomly(selectedEntrance);
            }

            return roomGenerated;
        }
        /// <summary>
        /// Places a room randomly by attempting to place a room at the given entrance and then giving up if no room can be placed.
        /// </summary>
        /// <param name="spawnEntrance">The entrance to attempt to spawn a room.</param>
        /// <returns>True if successful.</returns>
        private bool PlaceRoomRandomly(Entrance spawnEntrance)
        {
            if (!spawnEntrance) return false;

            HashSet<int> failedRandRooms = new HashSet<int>();
            Room selectedRoom = null;
            bool roomGenerated = false;
            while (!roomGenerated && !GenerationTimeUp)
            {
                selectedRoom = assetLib.GetRandomRoom(ref failedRandRooms);
                if (selectedRoom == null)
                {
                    spawnEntrance.CouldntFitAnything = true;
                    return false;
                }

                roomGenerated = PlaceRoom(selectedRoom, spawnEntrance);
            }

            return roomGenerated;
        }
        /// <summary>
        /// Attempt to place the given room at the given entrance.
        /// </summary>
        /// <param name="room">The room to be placed.</param>
        /// <param name="spawnEntrance">The entrance that the given room should be placed at.</param>
        /// <returns>True if successful.</returns>
        private bool PlaceRoom(Room room, Entrance spawnEntrance)
        {
            if (room == null || spawnEntrance.OtherEntrance != null) return false;

            if (roomsAnchor == null)
            {
                roomsAnchor = new GameObject("ROOMS_ANCHOR").transform;
                roomsAnchor.parent = transform;
            }

            Room generatedRoom = ObjectPoolManager.SINGLETON.Instantiate(room.gameObject, true, false).GetComponent<Room>();
            if (generatedRoom != null)
            {
                if (isDebug) generatedRoom.gameObject.name = $"{room.gameObject.name}_{generatedRooms.Count}";
                generatedRoom.Generator = this;
                if (generatedRoom.AttemptPlacement(spawnEntrance, canLinkWithOtherGenerators, this))
                {
                    generatedRoom.transform.parent = roomsAnchor;
                    generatedRooms.Add(generatedRoom);
                    generatedRoom.gameObject.SetActive(true);
                    return true;
                }
                else ObjectPoolManager.SINGLETON.Destroy(generatedRoom.gameObject);
            }

            return false;
        }

        /// <summary>
        /// Apply the states of the starting room entrances according to the onlyUseOneStartRoomEntrance option.
        /// </summary>
        private void ApplyEntranceStatesForStartingRoom()
        {
            List<Entrance> entrances = startingRoom.Entrances;
            foreach (Entrance entrance in entrances)
            {
                if (entrance.OtherEntrance != null && entrance != startingEntrance && entrance.OtherEntrance.Room.Generator == this)
                {
                    if (onlyUseOneStartRoomEntrance)
                    {
                        entrance.IsBlocked = true;
                        entrance.OtherEntrance.IsBlocked = true;
                        entrance.OtherEntrance.OtherEntrance = null;
                        entrance.OtherEntrance = null;
                    }
                    else
                    {
                        entrance.IsRoomGroupEntrance = true;
                        entrance.OtherEntrance.IsRoomGroupEntrance = true;
                    }
                }
            }
        }
        /// <summary>
        /// Apply the states for all the entrances on the outskirts of the generators that are connected to other generators according to the canLinkWithOtherGenerators option.
        /// </summary>
        private void ApplyEntranceStatesForOtherGeneratorLinks()
        {
            List<Entrance> entrances = LinkedEntrances;
            foreach (Entrance entrance in entrances)
            {
                if (entrance.IsBlocked || entrance.IsRoomGroupEntrance || entrance.Room == startingRoom || entrance.OtherEntrance.Room == startingRoom) continue;

                if (entrance.OtherEntrance.Room.Generator != this)
                {
                    if (canLinkWithOtherGenerators)
                    {
                        entrance.IsRoomGroupEntrance = true;
                        entrance.OtherEntrance.IsRoomGroupEntrance = true;
                    }
                    else
                    {
                        entrance.IsBlocked = true;
                        entrance.OtherEntrance.IsBlocked = true;
                        entrance.OtherEntrance.OtherEntrance = null;
                        entrance.OtherEntrance = null;
                    }
                }
            }
        }

        /// <summary>
        /// Check to see if all required values are set.
        /// </summary>
        private void CheckForMissingRequiredValues()
        {
            if (!assetLib) UnityLoggingUtility.LogMissingValue(GetType(), "assetLib", gameObject);
        }
        #endregion
        #endregion

        private void Awake()
        {
            CheckForMissingRequiredValues();
        }

        private void FixedUpdate()
        {
            if (!autoStarted && autoGenerate)
            {
                autoStarted = true;
                GenerateRooms();
            }
        }

        private void OnDrawGizmos()
        {
            if (isDebug)
            {
                if (startingRoom != null)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawSphere(startingRoom.transform.position, 3);
                    Gizmos.DrawLine(startingEntrance.transform.position, startingRoom.transform.position);
                }
            }
        }

        private void OnValidate()
        {
            if (maxRooms < 0) maxRooms = 0;
            if (maxGenerationTime < 0) maxGenerationTime = 0;
            if (roomSpawnTimeGap < 0) roomSpawnTimeGap = 0;
        }
    }

    [Serializable]
    public class RoomUnityEvent : UnityEvent<Room> { }
    [Serializable]
    public class EntranceUnityEvent : UnityEvent<Entrance> { }
}
