﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public static class RBMGDistanceUtility
    {
        /// <summary>
        /// Finds the farthest entrance from the given list from the given start room using FindAStarPathByRoom().
        /// </summary>
        /// <param name="startRoom">The start room.</param>
        /// <param name="entrances">The list of entrances to check.</param>
        /// <returns>The farthest entrance from the given list from the start room.</returns>
        public static Entrance FindFarthestEntranceByRoomAStar(Room startRoom, List<Entrance> entrances)
        {
            if (!startRoom || entrances == null || entrances.Count == 0) return null;

            int currFarthestDist = 0;
            Entrance currFarthestEntrance = null;
            int currDist = 0;
            foreach (Entrance entrance in entrances)
            {
                currDist = FindAStarPathByRoom(startRoom, entrance.Room).Count;
                if (currDist <= 0) continue;

                if (!currFarthestEntrance || currDist > currFarthestDist)
                {
                    currFarthestDist = currDist;
                    currFarthestEntrance = entrance;
                }
            }

            return currFarthestEntrance;
        }

        /// <summary>
        /// Finds a path by generator between the given start room and the given end room using the A-star pathfinding method.
        /// </summary>
        /// <param name="startRoom">The room to start at.</param>
        /// <param name="endRoom">The destination room.</param>
        /// <param name="AllowLink">The Predicate to run when determining if two linked rooms should be linked when processing path (optional).</param>
        /// <returns>The path in generators starting at the end.</returns>
        public static List<RandomRoomsGenerator> FindAStartPathByGenerator(Room startRoom, Room endRoom, Predicate<Entrance> AllowLink = null)
        {
            List<Room> roomPath = FindAStarPathByRoom(startRoom, endRoom, AllowLink);
            if (roomPath.Count == 0) return new List<RandomRoomsGenerator>();

            List<RandomRoomsGenerator> generatorPath = new List<RandomRoomsGenerator>();
            RandomRoomsGenerator currGenerator = roomPath[0].Generator;
            foreach(Room room in roomPath)
            {
                if (!currGenerator) continue;

                if (!generatorPath.Contains(currGenerator)) generatorPath.Add(currGenerator);
                else if (room.Generator != currGenerator) currGenerator = room.Generator;
            }

            return generatorPath;
        }
        /// <summary>
        /// Finds a path by Room between the given start room and the given end room using the A-star pathfinding method.
        /// </summary>
        /// <param name="startRoom">The room to start at.</param>
        /// <param name="endRoom">The destination room.</param>
        /// <param name="AllowLink">The Predicate to run when determining if two linked rooms should be linked when processing path (optional).</param>
        /// <returns>The path in Rooms starting at the end.</returns>
        public static List<Room> FindAStarPathByRoom(Room startRoom, Room endRoom, Predicate<Entrance> AllowLink = null)
        {
            // Followed video: https://youtu.be/-L-WgKMFuhE?t=455
            if (!startRoom || !endRoom) return new List<Room>();

            List<RoomAStarNode> openNodes = SetupAStarNodes(startRoom, endRoom, AllowLink);
            if (openNodes.Find(x => x.RepresentingRoom == endRoom) == null) return new List<Room>();
            List<RoomAStarNode> closedNodes = new List<RoomAStarNode>();
            
            RoomAStarNode currNode = null;
            while (currNode == null || currNode.RepresentingRoom != endRoom)
            {
                currNode = FindLowestFCost(openNodes);
                if (currNode == null) break;
                openNodes.Remove(currNode);
                closedNodes.Add(currNode);

                if (currNode.RepresentingRoom == endRoom) break;

                openNodes.ForEach(x => x.UpdateGCost(currNode.RepresentingRoom));
                currNode.NeighboringNodes.ForEach(x => x.ParentNode = currNode);
            }

            return BuildPathOfRooms(currNode);
        }

        /// <summary>
        /// Builds the path of Rooms by backtracking from the head of the A-star generated path.
        /// </summary>
        /// <param name="pathHeadNode">The head (final) node of the generated A-star path.</param>
        /// <returns>The path in Rooms starting at the end.</returns>
        private static List<Room> BuildPathOfRooms(RoomAStarNode pathHeadNode)
        {
            if (pathHeadNode == null || !pathHeadNode.RepresentingRoom || pathHeadNode.ParentNode == null) return new List<Room>();

            List<Room> foundPath = new List<Room>();
            RoomAStarNode currBacktrackNode = pathHeadNode;
            while (currBacktrackNode.ParentNode != null)
            {
                foundPath.Add(currBacktrackNode.RepresentingRoom);
                currBacktrackNode = currBacktrackNode.ParentNode;
            }
            foundPath.Add(currBacktrackNode.RepresentingRoom);
            return foundPath;
        }

        /// <summary>
        /// Starting at the start node, create a list of RoomAStarNodes going through all the neighbors until all rooms found have a node setup.
        /// </summary>
        /// <param name="startRoom">The starting room for pathfinding.</param>
        /// <param name="endRoom">The ending room for pathfinding.</param>
        /// <param name="AllowLink">The Predicate to run when determining if two linked rooms should be linked (optional).</param>
        /// <returns>A list of setup RoomAStarNodes of all rooms that have an eventual connection with the starting room.</returns>
        private static List<RoomAStarNode> SetupAStarNodes(Room startRoom, Room endRoom, Predicate<Entrance> AllowLink = null)
        {
            if (!startRoom || !endRoom) return new List<RoomAStarNode>();

            List<RoomAStarNode> setupNodes = new List<RoomAStarNode>();
            
            List<RoomAStarNode> currNewNodes = new List<RoomAStarNode>();
            currNewNodes.Add(new RoomAStarNode(startRoom, startRoom, endRoom));
            List<RoomAStarNode> newNodes = new List<RoomAStarNode>();
            while(currNewNodes.Count > 0)
            {
                foreach(RoomAStarNode node in currNewNodes)
                {
                    node.SetupNeighboringNodes(AllowLink);
                    setupNodes.Add(node);

                    foreach (RoomAStarNode neighborNode in node.NeighboringNodes)
                    {
                        if (setupNodes.Find(x => x.RepresentingRoom == neighborNode.RepresentingRoom) == null && newNodes.Find(x => x.RepresentingRoom == neighborNode.RepresentingRoom) == null)
                        {
                            newNodes.Add(neighborNode);
                        }
                    }
                }

                currNewNodes = newNodes;
                newNodes = new List<RoomAStarNode>();
            }

            return setupNodes;
        }

        /// <summary>
        /// Find the RoomAStarNode with the lowest FCost out of the given nodes.
        /// </summary>
        /// <param name="nodes">The nodes to look through.</param>
        /// <returns>The node with the lowest.</returns>
        private static RoomAStarNode FindLowestFCost(List<RoomAStarNode> nodes)
        {
            if (nodes == null || nodes.Count == 0) return null;

            RoomAStarNode currLowestNode = null;
            foreach(RoomAStarNode node in nodes)
            {
                if(currLowestNode == null || currLowestNode.FCost < node.FCost || (currLowestNode.FCost == node.FCost && currLowestNode.HCost < node.HCost)) currLowestNode = node;
            }

            return currLowestNode;
        }
    }

    public class RoomAStarNode
    {
        /// <summary>
        /// The nodes that represent this node's Room's neighboring rooms.
        /// </summary>
        public List<RoomAStarNode> NeighboringNodes => neighboringNodes;
        /// <summary>
        /// The distance this node's room is from the start or current node in a pathfinding process.
        /// </summary>
        public float GCost => gCost;
        /// <summary>
        /// The distance this node's room is from the end node in a pathfinding process.
        /// </summary>
        public float HCost => hCost;
        /// <summary>
        /// The sum of this node's G- and H-Cost.
        /// </summary>
        public float FCost => GCost + HCost;
        /// <summary>
        /// The previous node that this node came from in a pathfinding process.
        /// </summary>
        public RoomAStarNode ParentNode { get; set; }
        /// <summary>
        /// The Room that this node represents in a pathfinding process.
        /// </summary>
        public Room RepresentingRoom => representingRoom;

        private List<RoomAStarNode> neighboringNodes = new List<RoomAStarNode>();
        private float gCost = -1;
        private float startGCost = -1;
        private float hCost = -1;
        private Room representingRoom = null;
        private Room startRoom = null;
        private Room endRoom = null;

        /// <summary>
        /// Sets up the Room that this node represents and the costs values for the start and end rooms.
        /// </summary>
        /// <param name="room">The room this node represents of the pathfinding process.</param>
        /// <param name="startRoom">The starting room of the pathfinding process.</param>
        /// <param name="endRoom">The end room of the pathfinding process.</param>
        public RoomAStarNode(Room room, Room startRoom, Room endRoom)
        {
            if (!room || !startRoom || !endRoom) return;

            representingRoom = room;
            gCost = Vector3.Distance(startRoom.transform.position, representingRoom.transform.position);
            startGCost = GCost;
            hCost = Vector3.Distance(representingRoom.transform.position, endRoom.transform.position);
            this.startRoom = startRoom;
            this.endRoom = endRoom;
        }

        /// <summary>
        /// Set up the neighboring nodes of this node that represent the neighboring rooms of this node's Room.
        /// </summary>
        /// <param name="AllowLink">The Predicate to run when determining if two linked rooms should be linked (optional).</param>
        public void SetupNeighboringNodes(Predicate<Entrance> AllowLink = null)
        {
            RoomAStarNode currNeighboringNode = null;
            foreach (Entrance entrance in RepresentingRoom.Entrances)
            {
                if (!entrance.OtherEntrance || !entrance.OtherEntrance.Room || (AllowLink != null && !AllowLink(entrance))) continue;

                currNeighboringNode = new RoomAStarNode(entrance.OtherEntrance.Room, startRoom, endRoom);
                neighboringNodes.Add(currNeighboringNode);
            }
        }

        /// <summary>
        /// Update the current G-cost with the current room of the pathfinding process.
        /// </summary>
        /// <param name="currRoom">The new current room of the pathfinding process.</param>
        public void UpdateGCost(Room currRoom)
        {
            gCost = Vector3.Distance(currRoom.transform.position, representingRoom.transform.position);
        }
    }
}
