﻿namespace MattRGeorge.RoomBasedMapGenerator
{
    public interface IGeneratorRelatedObject
    {
        void ReceiveRandomRoomsGenerator(RandomRoomsGenerator generator);
        void ReceiveRandomRoomGroupsGenerator(RandomRoomGroupsGenerator generator);
    }
}
