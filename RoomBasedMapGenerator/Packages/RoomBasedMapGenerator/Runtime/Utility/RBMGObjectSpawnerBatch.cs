﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class RBMGObjectSpawnerBatch : MonoBehaviour
    {
        [Tooltip("The spawners that this batch component manages.")]
        [SerializeField] protected List<RBMGObjectSpawner> spawners = new List<RBMGObjectSpawner>();
        public UnityEvent OnBatchSpawnFinished = new UnityEvent();

        /// <summary>
        /// The spawners that this batch component manages.
        /// Get returns duplicate.
        /// </summary>
        public List<RBMGObjectSpawner> Spawners
        {
            get => new List<RBMGObjectSpawner>(spawners);
            set
            {
                if (value == null) return;
                spawners = ListUtility.RemoveNullUnityObjectEntries(value);
            }
        }

        /// <summary>
        /// Add a spawner to the list of managed spawners.
        /// </summary>
        /// <param name="spawner">The spawner to add.</param>
        public virtual void AddSpawner(RBMGObjectSpawner spawner)
        {
            if (!spawner) return;

            spawners.Add(spawner);
        }
        /// <summary>
        /// Remove a spawner to the list of managed spawners.
        /// </summary>
        /// <param name="spawner">The spawner to remove.</param>
        public virtual void RemoveSpawner(RBMGObjectSpawner spawner)
        {
            if (!spawner) return;

            spawners.Remove(spawner);
        }

        /// <summary>
        /// Have all the spawners this object manages spawn the rooms generator objects.
        /// </summary>
        public virtual void SpawnRoomsGeneratorObjects()
        {
            spawners.ForEach(x => x.SpawnRoomsGeneratorObjects());
            OnBatchSpawnFinished?.Invoke();
        }
        /// <summary>
        /// Have all the spawners this object manages spawn the room groups generator objects.
        /// </summary>
        public virtual void SpawnRoomGroupsGeneratorObjects()
        {
            spawners.ForEach(x => x.SpawnRoomGroupsGeneratorObjects());
            OnBatchSpawnFinished?.Invoke();
        }

        protected virtual void OnValidate()
        {
            Spawners = spawners;
        }
    }
}
