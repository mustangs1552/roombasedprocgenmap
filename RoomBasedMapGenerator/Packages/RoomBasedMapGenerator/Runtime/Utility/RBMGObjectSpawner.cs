﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.RoomBasedMapGenerator
{
    public class RBMGObjectSpawner : ObjectSpawnerViaAssetLibrary
    {
        [Tooltip("The default rooms generator to use for spawning.")]
        [SerializeField] protected RandomRoomsGenerator defaultRoomsGenerator = null;
        [Tooltip("The default room groups generator to use for spawning.")]
        [SerializeField] protected RandomRoomGroupsGenerator defaultRoomGroupsGenerator = null;
        [Tooltip("Include the start room's nodes when spawning objects?")]
        public bool includeStartRoomsNodes = true;

        /// <summary>
        /// The default rooms generator to use for spawning.
        /// </summary>
        public RandomRoomsGenerator DefaultRoomsGenerator
        {
            get => defaultRoomsGenerator;
            set
            {
                if (!value) return;
                defaultRoomsGenerator = value;
            }
        }
        /// <summary>
        /// The default room groups generator to use for spawning.
        /// </summary>
        public RandomRoomGroupsGenerator DefaultRoomGroupsGenerator
        {
            get => defaultRoomGroupsGenerator;
            set
            {
                if (!value) return;
                defaultRoomGroupsGenerator = value;
            }
        }

        /// <summary>
        /// Spawn objects at the given or default rooms generator nodes.
        /// </summary>
        /// <param name="generator">The custom rooms generator to use.</param>
        public virtual void SpawnRoomsGeneratorObjects(RandomRoomsGenerator generator = null)
        {
            if (!generator && !DefaultRoomsGenerator) return;
            if (!generator && DefaultRoomsGenerator) generator = DefaultRoomsGenerator;

            if (includeStartRoomsNodes) SpawnObjects(generator.ObjSpawnNodes, -1);
            else SpawnObjects(generator.ObjSpawnNodesWithoutStartRoom, -1);
        }
        /// <summary>
        /// Spawn objects at the given or default room groups generator nodes.
        /// </summary>
        /// <param name="generator">The custom room groups generator to use.</param>
        public virtual void SpawnRoomGroupsGeneratorObjects(RandomRoomGroupsGenerator generator = null)
        {
            if (!generator && !DefaultRoomGroupsGenerator) return;
            if (!generator && DefaultRoomGroupsGenerator) generator = DefaultRoomGroupsGenerator;

            if (includeStartRoomsNodes) SpawnObjects(generator.ObjSpawnNodes, -1);
            else SpawnObjects(generator.ObjSpawnNodesWithoutStartRoom, -1);
        }
    }
}
