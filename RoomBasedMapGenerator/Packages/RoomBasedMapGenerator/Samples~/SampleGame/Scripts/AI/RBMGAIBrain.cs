﻿using MattRGeorge.AI.AIIndividual;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class RBMGAIBrain : AIBrain, IGeneratorRelatedObject
    {
        protected RandomRoomGroupsGenerator groupsGenerator = null;
        protected RandomRoomsGenerator roomsGenerator = null;

        public RandomRoomGroupsGenerator GroupsGenerator => groupsGenerator;
        public RandomRoomsGenerator RoomsGenerator => roomsGenerator;

        /// <summary>
        /// Get the number of room groups this AI was spawned in away from the start room of this AI's room group generator.
        /// </summary>
        /// <returns>Number of room groups away from starting room of room group generator.</returns>
        public virtual int GetRoomGroupsDepth()
        {
            if (!GroupsGenerator || !RoomsGenerator) return 0;

            return RBMGDistanceUtility.FindAStartPathByGenerator(groupsGenerator.StartingEntrance.Room, RoomsGenerator.GeneratedRooms[0]).Count;
        }

        public virtual void Kill()
        {
            ObjectPoolManager.SINGLETON.Destroy(gameObject);
        }

        public void ReceiveRandomRoomGroupsGenerator(RandomRoomGroupsGenerator generator)
        {
            if (!generator) return;

            groupsGenerator = generator;
        }

        public void ReceiveRandomRoomsGenerator(RandomRoomsGenerator generator)
        {
            if (!generator) return;

            roomsGenerator = generator;
        }

        public override void OnDestroyedByObjectPool()
        {
            base.OnDestroyedByObjectPool();

            groupsGenerator = null;
            roomsGenerator = null;
        }
    }
}
