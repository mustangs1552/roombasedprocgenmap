﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class ScoreController : MonoBehaviour
    {
        public static ScoreController SINGLETON = null;
        [Tooltip("Erase the current saved high score from player prefs on start?")]
        public bool eraseHighScoreOnStart = false;
        [Tooltip("Enable debugging?")]
        public bool isDebug = false;
        [Tooltip("The UI text objects to show the high score value.")]
        [SerializeField] protected List<TextMeshProUGUI> highScoreTexts = new List<TextMeshProUGUI>();
        [Tooltip("The UI text objects to show the current score value.")]
        [SerializeField] protected List<TextMeshProUGUI> currScoreTexts = new List<TextMeshProUGUI>();

        protected const string savedHighScoreName = "playerHighScore";
        protected const string savedCurrScore = "playerCurrScore";

        /// <summary>
        /// The saved high score value.
        /// </summary>
        public int HighScore
        {
            get => PlayerPrefs.GetInt(savedHighScoreName, 0);
            set
            {
                if (value < 0 || HighScore >= value) return;
                PlayerPrefs.SetInt(savedHighScoreName, value);
                if (highScoreTexts != null) highScoreTexts.ForEach(x => x.text = value.ToString());
                if (isDebug) Debug.Log($"New highscore: {value}");
            }
        }
        /// <summary>
        /// The saved current score value.
        /// </summary>
        public int CurrScore
        {
            get => PlayerPrefs.GetInt(savedCurrScore, 0);
            set
            {
                if (value < 0) return;
                PlayerPrefs.SetInt(savedCurrScore, value);
                if (currScoreTexts != null) currScoreTexts.ForEach(x => x.text = value.ToString());
                if (isDebug) Debug.Log($"New current score: {value}");
                HighScore = value;
            }
        }

        /// <summary>
        /// Update the UI text objects with the current saved score values.
        /// </summary>
        public virtual void UpdateUI()
        {
            if (highScoreTexts != null) highScoreTexts.ForEach(x => x.text = HighScore.ToString());
            if (currScoreTexts != null) currScoreTexts.ForEach(x => x.text = CurrScore.ToString());
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            UpdateUI();
        }

        protected virtual void Awake()
        {
            if (!ScoreController.SINGLETON) SINGLETON = this;
            else
            {
                Destroy(gameObject);
                return;
            }

            SceneManager.sceneLoaded += OnSceneLoaded;
            DontDestroyOnLoad(this);

            CurrScore = 0;
            if (eraseHighScoreOnStart) PlayerPrefs.DeleteKey(savedHighScoreName);
            UpdateUI();
        }
    }
}
