﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Objects;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class GoalRoom : MonoBehaviour
    {
        [Tooltip("The 'Room' component on this goal room.")]
        [SerializeField] protected Room goalRoomRoomComp = null;
        [Tooltip("The generator that generated the map this room should spawn in.")]
        [SerializeField] protected RandomRoomGroupsGenerator generator = null;
        [Tooltip("The door object to be spawned at this room's entrance when spawned.")]
        [SerializeField] protected GameObject door = null;
        [Tooltip("The key to spawn for the door of this room.")]
        [SerializeField] protected UsableKey key = null;

        protected List<Entrance> failedEntrances = new List<Entrance>();
        protected Door spawnedEntranceDoor = null;

        /// <summary>
        /// Position this room at an entrance in the assigned generator.
        /// </summary>
        public virtual void PositionRoom()
        {
            if (!generator || !door || !generator.StartingEntrance || !generator.StartingEntrance.Room) return;
            failedEntrances = new List<Entrance>();

            Entrance entrance = FindValidEntrance();
            while (entrance)
            {
                if (goalRoomRoomComp.AttemptPlacement(entrance))
                {
                    SpawnDoor(entrance);
                    gameObject.SetActive(true);
                    break;
                }
                else
                {
                    failedEntrances.Add(entrance);
                    entrance = FindValidEntrance();
                }
            }
        }

        /// <summary>
        /// A key was spawned.
        /// Check to see if it was the key for this goal room and link up the key with the spawned door.
        /// </summary>
        /// <param name="spawnedKey">The key object that was spawned.</param>
        public virtual void OnKeySpawned(GameObject spawnedKey)
        {
            if (!spawnedKey || !spawnedEntranceDoor) return;
            UsableKey spawnedUsableKey = spawnedKey.GetComponent<UsableKey>();
            if (!spawnedUsableKey) return;

            if (key.gameObject.name == GameObjectUtility.GetActualName(spawnedKey.gameObject)) spawnedEntranceDoor.lockObj.LinkRequiredKey(spawnedUsableKey);
        }

        /// <summary>
        /// Spawn and setup the door object.
        /// </summary>
        /// <param name="entrance">The entrance this room was spawned at.</param>
        protected void SpawnDoor(Entrance entrance)
        {
            if (!entrance || !door || !KeySpawner.SINGLETON) return;

            entrance.PlaceEntranceObject(door);
            if (!key || !entrance.EntranceObject) return;
            spawnedEntranceDoor = entrance.EntranceObject.GetComponent<Door>();
            if (spawnedEntranceDoor == null) return;
            KeySpawner.SINGLETON.SpawnKey(entrance, key);
        }

        /// <summary>
        /// Find a valid entrance that this room can spawn at.
        /// </summary>
        /// <returns>A valid room.</returns>
        protected virtual Entrance FindValidEntrance()
        {
            if (!generator) return null;

            List<Entrance> entrances = generator.UnlinkedUnblockedEntrances;
            if (entrances == null || entrances.Count == 0) return null;
            failedEntrances.ForEach(x => entrances.Remove(x));
            if (entrances.Count == 0) return null;
            return RBMGDistanceUtility.FindFarthestEntranceByRoomAStar(generator.StartingEntrance.Room, generator.UnlinkedUnblockedEntrances);
        }

        /// <summary>
        /// Check for any missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!goalRoomRoomComp)
            {
                goalRoomRoomComp = GetComponent<Room>();
                if (!goalRoomRoomComp) UnityLoggingUtility.LogMissingValue(GetType(), "goalRoomRoomComp", gameObject);
            }
            if (!generator) UnityLoggingUtility.LogMissingValue(GetType(), "generator", gameObject);
            if (!door) UnityLoggingUtility.LogMissingValue(GetType(), "door", gameObject);
        }

        protected virtual void Start()
        {
            CheckMissingValues();
            gameObject.SetActive(false);
        }
    }
}
