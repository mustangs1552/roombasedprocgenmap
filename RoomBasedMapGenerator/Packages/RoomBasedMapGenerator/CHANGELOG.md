# Changelog

Format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.7.0] - 7/20/20
### [Added]
- A simple sample game where the player navigates a randomly generated map with locked doors and enemy suicide bots.
- Can now specify a distance from the current entrance when checking for clearance for a new room group.
- RandomRoomsGenerator.StartingEntrance to get access to the starting entrance of that generator.
- RandomRoomGroupsGenerator.StartingEntrance to get access to the starting entrance of that generator.
- RandomRoomGroupsGenerator.Generators to get access to each of the spawned RandomRoomsGenerators.
- Entrance.PlaceEntranceObject() that spawns the given object as the entrance object.
- RandomRoomsGenerator.ObjSpawnNodesWithoutStartRoom and RandomRoomGroupsGenerator.ObjSpawnNodesWithoutStartRoom to get object spawn nodes from generator's rooms without the starting room included.
- Added RBMGObjectSpawnerBatch that can call RBMGObjectSpawners as a batch.
### [Changed]
- Updated basic samples with chanegs made for sample game.

## [0.6.0] - [5/20/20]
### [Added]
- Room templates to the samples that can have prefab varients for different rooms that use the template mesh.
- An Entrance prefab to the samples that each of the Room sampple prefabs use.
### [Changed]
- Samples folder structure.
- Room prefabs names, Entrances are now the new Entrance prefab, and the Rooms are now prefab variants of their Room template.

## [0.5.0] - [5/19/20]
### [Added]
- CanceledGeneration to the generators that is true when the generator canceled its last generation for some reason.
- Option to have the RandomRoomGroupsGenerator to add the next group at the farthest entrance from the start.
- RBMGDistanceUtility.FindFarthestEntranceByRoomAStar() that finds the farthest entrance via the find A-star path method.
- Added option to toggle the numbers appended to the end of spawned room names.

## [0.4.3] - [5/17/20]
### [Added]
- RoomAAA which is the same as RoomA but, rotated 45 degrees.
- RoomE which is a more complicated room that has non-90 degree entrances and is odd shaped.
### [Fixed]
- Non-90 degree entrance placement issues.
- Improved spawning quality further. RandomRoomsGenerator is now delayed by roomSpawnTimeGap.
### [Changed]
- Generators' auto start now gets called within FixedUpdate().

## [0.4.0] - 5/14/20
### [Added]
- Ability to provide a Predicate to use when determining what Entrances are linked when processing a path via DistanceUtility.
- RoomD and RoomB to the samples folder and scenes.
- Ability to set custom layer for room placement colliders on rooms.
- .1 second delay between placment of each room when generating map. This has improved collision detection when placing the rooms.
- New debug lines to show what room colliders are intersecting.
### [Changed]
- "GeneratorRoom" layer to "GeneratorRoomCollider" since it only needs to be on the placement colliders and not the entire room.
- Gizmo size of drawn placement colliders for rooms to make it easier to see around it.
- Room placement colliders must now be there own child objects under their room and their local positions must be realative to the parent Room object.
### [Fixed]
- RoomD upper floor entrances linking to other entrances improperly. Entrance local positions must be realative to the parent Room object. Updated offset entrance error to mention this design requirement.
- RoomB odd shaped room placement.

## [0.3.1] - 4/28/20
### [Fixed]
- Samples folder behaviour.

## [0.3.0] - 4/28/20
### [Added]
- RBMGDistanceUtitily that can use A-star pathfinding to find a path by room between a start and end room which can be used to get the number of rooms between the two.
### [Fixed]
- Some missing references in sample assets.

## [0.2.0] - 4/26/20
### [Changed]
- The namespaces to be in its own package.
- Renamed RoomGeneratorAssetLibrary to a shorter and after the package RBMGAssetLibrary (RoomBasedMapGeneratorAssetLibrary).
### [Fixed]
- Hardcoded value for max objects to spawn in RandomRoomsGenerator.

## [0.1.2] - 4/23/20
### [Added]
- Samples with basic rooms and objects. One scene for RandomRoomsGenerator and another for RandomRoomGroupsGenerator.
- Option to auto generate on start to the generators.

## [0.1.0] - 4/22/20
### [Changed]
- Replaced some while loops with coroutines and added a timer to cancel generation if something gets stuck.
### [Moved]
- Project to its own package and published with "preview" status.

## [0.0.2] - 4/21/20
### [Changed]
- Improved value control and error posting.