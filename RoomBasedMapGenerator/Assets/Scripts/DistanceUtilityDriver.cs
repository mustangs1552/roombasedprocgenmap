﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.RoomBasedMapGenerator;

namespace Assets.Scripts
{
    public class DistanceUtilityDriver : MonoBehaviour
    {
        public bool sameGroupOnly = false;
        public Room startRoom = null;
        public Room endRoom = null;

        private List<Room> lastPath = new List<Room>();

        private bool IsDiffGroup(Entrance entrance)
        {
            if (!entrance) return false;

            return !entrance.IsRoomGroupEntrance;
        }

        private void OnValidate()
        {
            if (!startRoom || !endRoom) return;

            lastPath = (sameGroupOnly) ? RBMGDistanceUtility.FindAStarPathByRoom(startRoom, endRoom, IsDiffGroup) : RBMGDistanceUtility.FindAStarPathByRoom(startRoom, endRoom);
        }

        private void OnDrawGizmos()
        {
            if (lastPath == null || lastPath.Count == 0) return;

            for(int i = 0; i < lastPath.Count; i++)
            {
                if (i + 1 >= lastPath.Count) break;

                Gizmos.DrawLine(lastPath[i].transform.position, lastPath[i + 1].transform.position);
            }
        }
    }
}
