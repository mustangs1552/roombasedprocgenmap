﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.AI.AIIndividual;

namespace MattRGeorge.AI.Samples.Traits
{
    public class RandomWanderTrait : AITrait
    {
        [Tooltip("The min and max x-axis values for the next wander point in relation to this object's position.")]
        [SerializeField] protected Vector2 wanderMinMaxRangeX = new Vector2(-10, 10);
        [Tooltip("The min and max z-axis values for the next wander point in relation to this object's position.")]
        [SerializeField] protected Vector2 wanderMinMaxRangeZ = new Vector2(-10, 10);
        [Tooltip("The min and max values for the cooldown before choosing the next wander point.")]
        [SerializeField] protected Vector2 nextMoveMinMaxCooldown = new Vector2(5, 10);
        [Tooltip("Choose the next wander point when the current one was reached?")]
        public bool chooseNewPathWhenReached = true;

        /// <summary>
        /// The min and max x-axis values for the next wander point in relation to this object's position.
        /// </summary>
        public Vector2 WanderMinMaxRangeX
        {
            get => new Vector2(wanderMinMaxRangeX.x, wanderMinMaxRangeX.y);
            set
            {
                float x = value.x;
                float y = value.y;
                if (x > y) x = y;

                wanderMinMaxRangeX = new Vector2(x, y);
            }
        }
        /// <summary>
        /// The min and max z-axis values for the next wander point in relation to this object's position.
        /// </summary>
        public Vector2 WanderMinMaxRangeZ
        {
            get => new Vector2(wanderMinMaxRangeZ.x, wanderMinMaxRangeZ.y);
            set
            {
                float x = value.x;
                float y = value.y;
                if (x > y) x = y;

                wanderMinMaxRangeZ = new Vector2(x, y);
            }
        }
        /// <summary>
        /// The min and max values for the cooldown before choosing the next wander point.
        /// </summary>
        public Vector2 NextMoveMinMaxCooldown
        {
            get => new Vector2(nextMoveMinMaxCooldown.x, nextMoveMinMaxCooldown.y);
            set
            {
                float x = (value.x < 0) ? 0 : value.x;
                float y = (value.y < 0) ? 0 : value.y;
                if (x > y) x = y;

                nextMoveMinMaxCooldown = new Vector2(x, y);
            }
        }

        protected MovementTrait movementTrait = null;
        protected bool readyForNextMove = true;
        protected Coroutine timeTilNextMove = null;

        public override bool Checking()
        {
            return readyForNextMove && movementTrait;
        }

        public override bool Performing()
        {
            return SetNewWanderPoint();
        }

        public override bool CancelPerform()
        {
            TraitReset();
            return true;
        }

        public override void PreTraitSetup()
        {
            
        }
        public override void TraitSetup()
        {
            movementTrait = Brain.GetTrait<MovementTrait>();
            if (!movementTrait) UnityLoggingUtility.LogMissingValue(GetType(), "movementTrait", gameObject);
        }
        public override void PostTraitSetup()
        {
            
        }

        public override void TraitReset()
        {
            readyForNextMove = true;
            if (timeTilNextMove != null) StopCoroutine(timeTilNextMove);
        }

        /// <summary>
        /// Choose the next wander point.
        /// </summary>
        /// <returns>The next wander point.</returns>
        protected virtual Vector3 ChoosePoint()
        {
            float wanderRangeX = Random.Range(WanderMinMaxRangeX.x, WanderMinMaxRangeX.y);
            float wanderRangeZ = Random.Range(WanderMinMaxRangeZ.x, WanderMinMaxRangeZ.y);
            return new Vector3(transform.position.x + wanderRangeX, transform.position.y, transform.position.z + wanderRangeZ);
        }
        /// <summary>
        /// Choose the next cooldown before the next wander point.
        /// </summary>
        /// <returns>The next cooldown.</returns>
        protected virtual float ChooseCooldown()
        {
            if (timeTilNextMove != null) StopCoroutine(timeTilNextMove);
            return Random.Range(NextMoveMinMaxCooldown.x, NextMoveMinMaxCooldown.y);
        }
        /// <summary>
        /// Choose the next wander point and cooldown and set it in the movement trait.
        /// </summary>
        protected virtual bool SetNewWanderPoint()
        {
            if (!movementTrait) return false;

            movementTrait.SetDestination(ChoosePoint());
            timeTilNextMove = StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(OnCooldownEnded, ChooseCooldown()));

            readyForNextMove = false;
            return false;
        }

        /// <summary>
        /// The current cooldown was up.
        /// </summary>
        protected virtual void OnCooldownEnded()
        {
            TraitReset();
        }
        /// <summary>
        /// The movement trait ended movement.
        /// </summary>
        /// <param name="successful">Movement trait successfully reached the destination.</param>
        protected virtual void OnMovementEnd(bool successful)
        {
            if (!chooseNewPathWhenReached) return;

            TraitReset();
        }

        protected virtual void OnValidate()
        {
            WanderMinMaxRangeX = wanderMinMaxRangeX;
            WanderMinMaxRangeZ = wanderMinMaxRangeZ;
            NextMoveMinMaxCooldown = nextMoveMinMaxCooldown;
        }
    }
}
