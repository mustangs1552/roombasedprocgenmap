﻿using System;
using UnityEngine;
using UnityEngine.AI;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.AI.AIIndividual;

namespace MattRGeorge.AI.Samples.Traits
{
    public class MovementTrait : PassiveTrait
    {
        [Tooltip("The navigation mesh agent that belongs to this AI.")]
        [SerializeField] protected NavMeshAgent navAgent = null;
        [Tooltip("Enable debugging?")]
        public bool isDebug = false;

        protected Vector3 currTarget = Vector3.zero;
        protected Action<bool> CurrOnEnd = null;

        /// <summary>
        /// Set the destination target to the navigation mesh agent and start moving.
        /// </summary>
        /// <param name="target">The destiantion target.</param>
        /// <param name="OnEnd">An event that is called when movement ends, either the destination was reched or canceled.</param>
        public virtual void SetDestination(Vector3 target, Action<bool> OnEnd = null)
        {
            if (!navAgent) return;

            currTarget = target;
            navAgent.SetDestination(target);
            CurrOnEnd = OnEnd;
        }

        /// <summary>
        /// Cancel the current destination target.
        /// </summary>
        public virtual void CancelDestination()
        {
            if (!navAgent) return;

            currTarget = Vector3.zero;
            navAgent.ResetPath();
            CurrOnEnd?.Invoke(false);
            CurrOnEnd = null;
        }

        /// <summary>
        /// Pause the navigation mesh agent's movement.
        /// </summary>
        public virtual void Stop()
        {
            if (!navAgent) return;

            navAgent.isStopped = true;
        }
        /// <summary>
        /// Resume the navigation mesh agent's movement.
        /// </summary>
        public virtual void Resume()
        {
            if (!navAgent) return;

            navAgent.isStopped = false;
        }

        /// <summary>
        /// Check if the current target destination was reached.
        /// Will call the stored event if there is one if it was reached.
        /// </summary>
        /// <returns>True if the current target destination was reached.</returns>
        public virtual bool CheckReached()
        {
            if (!navAgent) return false;

            float currDist = Vector3.Distance(transform.position, currTarget);
            if (currDist <= navAgent.stoppingDistance || !navAgent.hasPath)
            {
                CurrOnEnd?.Invoke(true);
                return true;
            }

            return false;
        }

        public override void PreTraitSetup()
        {
            CheckMissingValues();
        }
        public override void TraitSetup()
        {
            
        }
        public override void PostTraitSetup()
        {
            
        }

        public override void TraitReset()
        {
            CancelDestination();
        }

        public override void UpdateTrait()
        {
            CheckReached();
        }

        /// <summary>
        /// Check for any missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!navAgent) UnityLoggingUtility.LogMissingValue(GetType(), "navAgent", gameObject);
        }

        protected virtual void OnDrawGizmos()
        {
            if (!isDebug) return;

            if (currTarget != Vector3.zero && navAgent)
            {
                Gizmos.color = (navAgent.isStopped) ? Color.red : Color.green;
                NavMeshPath path = navAgent.path;
                Vector3 lastPoint = transform.position;
                foreach(Vector3 point in path.corners)
                {
                    Gizmos.DrawLine(lastPoint, point);
                    lastPoint = point;
                }
            }
        }
    }
}
