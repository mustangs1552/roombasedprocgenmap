﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.AI.Samples.Traits
{
    /// <summary>
    /// This sense is the AI's ability to smell other objects.
    /// This looks for Scent objects which represents a smells.
    /// </summary>
    public class SmellSense : Sense
    {
        [Tooltip("The range that this AI can smell a scent's range.")]
        [SerializeField] protected float range = 10;
        [Tooltip("The accuracy of this AI's ability to detect a scent.")]
        [SerializeField, Range(0, 1)] protected float accuracy = .75f;
        [Tooltip("Enable debugging?")]
        public bool isDebug = false;
        [Tooltip("The frequency the sense test is ran.")]
        public float testFreq = 1;

        /// <summary>
        /// The range that this AI can smell a scent's range.
        /// </summary>
        public float Range
        {
            get => range;
            set
            {
                range = (value < 0) ? 0 : value;
            }
        }
        /// <summary>
        /// The accuracy of this AI's ability to detect a scent.
        /// </summary>
        public float Accuracy
        {
            get => accuracy;
            set
            {
                if (value < 0) accuracy = 0;
                else if (value > 1) accuracy = 1;
                else accuracy = value;
            }
        }

        protected float lastTest = 0;
        protected List<Transform> lastTestObjects = new List<Transform>();

        #region Methods
        public override List<Transform> CheckSense()
        {
            return CheckSmell(FindValidObjects());
        }
        public override List<Transform> CheckSense(List<Transform> objs)
        {
            return CheckSmell(objs);
        }

        public override void SenseReset()
        {
            
        }
        public override void SenseSetup()
        {
            
        }

        /// <summary>
        /// Calculate the accuracy for sensing the given scent.
        /// </summary>
        /// <param name="scent">The scent to check the accuracy of.</param>
        /// <returns>A value between 0 and 1 representing the accuracy.</returns>
        protected virtual float CalcAccuracy(Scent scent)
        {
            if (!scent) return 0;

            float dist = Vector3.Distance(transform.position, scent.transform.position);
            float maxDist = Range + scent.Range;
            if (dist >= maxDist) return 0;
            else return (Accuracy == 1) ? 1 : (((maxDist - dist) / maxDist) + Accuracy) / 2;
        }
        /// <summary>
        /// Check the given list of scents to see if this creature can smell any of them.
        /// </summary>
        /// <param name="scents">The scents to check.</param>
        /// <returns>The scents' transfoms that this creature smells.</returns>
        protected virtual List<Transform> CheckSmell(List<Transform> scents)
        {
            if (scents == null || scents.Count == 0) return new List<Transform>();

            List<Transform> sensedScents = new List<Transform>();
            foreach(Transform scentTrans in scents)
            {
                Scent scent = scentTrans.GetComponent<Scent>();
                if (!scent) continue;

                float currAccuracy = CalcAccuracy(scent);
                if (currAccuracy <= 0) continue;
                else if (currAccuracy >= 1) sensedScents.Add(scentTrans);
                else
                {
                    float randNum = Random.Range(0f, 100f) / 100f;
                    if (currAccuracy >= randNum) sensedScents.Add(scentTrans);
                }
            }

            return sensedScents;
        }

        /// <summary>
        /// Find all the valid objects that this AI can sense.
        /// </summary>
        /// <returns>A list of valid sensable objects.</returns>
        protected virtual List<Transform> FindValidObjects()
        {
            if (!ObjectPoolManager.SINGLETON) return new List<Transform>();

            List<Scent> scents = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<Scent>();
            List<Transform> validObjs = new List<Transform>();
            scents.ForEach(x => validObjs.Add(x.transform));

            validObjs = CheckObjectsAndAddAlwaysCheckObjects(validObjs);

            return validObjs;
        }

        /// <summary>
        /// Runs a test that gets all sensed objects and shows the results via debug lines connecting those objects.
        /// Green: Did sense
        ///   Red: Did not sense
        /// </summary>
        protected virtual void SenseTest()
        {
            List<Transform> startingObjs = FindValidObjects();
            List<Transform> endingObjs = lastTestObjects;
            if (Time.time - lastTest >= testFreq)
            {
                endingObjs = CheckSmell(startingObjs);
                lastTestObjects = endingObjs;
                lastTest = Time.time;
            }

            foreach (Transform obj in startingObjs)
            {
                if (obj.gameObject.layer != (int)LayerMask.NameToLayer("Ignore Raycast")) Debug.DrawLine(transform.position, obj.position, (endingObjs.Contains(obj)) ? Color.green : Color.red);
            }
        }
        #endregion

        protected override void OnValidate()
        {
            base.OnValidate();

            Range = range;
            Accuracy = accuracy;
        }

        protected virtual void OnDrawGizmos()
        {
            if (isDebug)
            {
                SenseTest();

                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, Range);
            }
        }
    }
}
