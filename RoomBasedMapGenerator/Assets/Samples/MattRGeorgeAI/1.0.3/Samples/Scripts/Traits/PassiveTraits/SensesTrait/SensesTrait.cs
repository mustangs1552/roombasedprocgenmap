﻿using UnityEngine;
using System;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.AI.AIIndividual;

namespace MattRGeorge.AI.Samples.Traits
{
    /// <summary>
    /// The main script that accesses the various senses of this AI.
    /// </summary>
    public class SensesTrait : PassiveTrait
    {
        [Tooltip("The senses that belongs to this this trait.")]
        [SerializeField] private List<Sense> senses = new List<Sense>();

        /// <summary>
        /// The senses that belongs to this trait.
        /// Get returns duplicate.
        /// </summary>
        public List<Sense> Senses
        {
            get => new List<Sense>(senses);
            set
            {
                if (value == null) senses = new List<Sense>();
                else senses = ListUtility.RemoveNullUnityObjectEntries(value);
            }
        }

        public override void PreTraitSetup()
        {
            CheckMissingValues();
        }
        public override void TraitSetup()
        {

        }
        public override void PostTraitSetup()
        {

        }

        public override void UpdateTrait()
        {

        }

        public override void TraitReset()
        {
            ResetSenses();
        }

        /// <summary>
        /// Gets all the objects currently sensed by all senses.
        /// </summary>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public virtual List<Transform> CheckSenses()
        {
            List<Transform> objs = new List<Transform>();

            List<Transform> currObjs = new List<Transform>();
            foreach (Sense sense in Senses)
            {
                currObjs = CheckSense(sense.GetType());
                currObjs.ForEach(x => objs.Add(x));
            }

            return objs;
        }
        /// <summary>
        /// Gets all the objects in the given list currently sensed by all senses.
        /// </summary>
        /// <param name="objs">The list of objects to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public virtual List<Transform> CheckSenses(List<Transform> objs)
        {
            List<Transform> sensedObjs = new List<Transform>();
            foreach (Sense sense in Senses)
            {
                sense.CheckSense(objs).ForEach(x => sensedObjs.Add(x));
            }

            return sensedObjs;
        }

        /// <summary>
        /// Gets all the objects currently sensed by the desired sense.
        /// </summary>
        /// <param name="type">The typeof() sense to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public virtual List<Transform> CheckSense(Type type)
        {
            List<Transform> objs = new List<Transform>();

            foreach (Sense sense in Senses)
            {
                if (type == sense.GetType()) objs = sense.CheckSense();
            }

            return objs;
        }
        /// <summary>
        /// Gets all the objects in the given list currently sensed by the desired sense.
        /// </summary>
        /// <param name="type">The typeof() sense to check.</param>
        /// <param name="objs">The list of objects to check.</param>
        /// <returns>A list of sensed objects as Transforms.</returns>
        public virtual List<Transform> CheckSense(Type type, List<Transform> objs)
        {
            List<Transform> sensedObjs = new List<Transform>();
            foreach (Sense sense in Senses)
            {
                if (type == sense.GetType()) sense.CheckSense(objs).ForEach(x => sensedObjs.Add(x));
            }

            return sensedObjs;
        }

        /// <summary>
        /// Reset the SensesTrait and Senses.
        /// </summary>
        public virtual void ResetSenses()
        {
            Senses.ForEach(x => x.SenseReset());
        }

        /// <summary>
        /// Check for missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (Senses == null || Senses.Count == 0) UnityLoggingUtility.LogMissingValue(GetType(), "senses", gameObject);
        }

        protected virtual void OnValidate()
        {
            Senses = senses;
        }
    }
}