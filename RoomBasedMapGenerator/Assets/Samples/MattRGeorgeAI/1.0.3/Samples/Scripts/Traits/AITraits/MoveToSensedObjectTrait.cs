﻿using UnityEngine;
using MattRGeorge.AI.AIIndividual;
using MattRGeorge.Unity.Utilities.Static;
using System.Collections.Generic;

namespace MattRGeorge.AI.Samples.Traits
{
    public class MoveToSensedObjectTrait : AITrait
    {
        [Tooltip("Cooldown after an object was reached or trait was canceled.")]
        [SerializeField] protected float cooldown = 5;

        /// <summary>
        /// Cooldown after an object was reached or trait was canceled.
        /// </summary>
        public float Cooldown
        {
            get => cooldown;
            set
            {
                cooldown = (value < 0) ? 0 : value;
            }
        }

        protected MovementTrait movementTrait = null;
        protected SensesTrait sensesTrait = null;
        protected bool readyForNextMove = true;
        protected Coroutine cooldownCoroutine = null;
        protected bool isMoving = false;

        public override bool Checking()
        {
            return movementTrait && sensesTrait && (readyForNextMove || isMoving);
        }

        public override bool Performing()
        {
            if (!isMoving) StartMoving();
            return isMoving;
        }
        public override bool CancelPerform()
        {
            if(!movementTrait) 

            if (cooldownCoroutine == null) StartCooldown();
            movementTrait.CancelDestination();

            return true;
        }

        public override void PreTraitSetup()
        {
            
        }
        public override void TraitSetup()
        {
            movementTrait = Brain.GetTrait<MovementTrait>();
            if (!movementTrait) UnityLoggingUtility.LogMissingValue(GetType(), "movementTrait", gameObject);

            sensesTrait = Brain.GetTrait<SensesTrait>();
            if (!movementTrait) UnityLoggingUtility.LogMissingValue(GetType(), "sensesTrait", gameObject);
        }
        public override void PostTraitSetup()
        {
            
        }

        public override void TraitReset()
        {
            if (!movementTrait) return;

            if (cooldownCoroutine != null) StopCoroutine(cooldownCoroutine);
            movementTrait.CancelDestination();
            readyForNextMove = true;
        }

        /// <summary>
        /// Find a sensed object and start moving towards it.
        /// </summary>
        protected virtual void StartMoving()
        {
            if (!movementTrait || !sensesTrait) return;

            List<Transform> sensedObjs = sensesTrait.CheckSenses();
            if (sensedObjs == null || sensedObjs.Count == 0)
            {
                readyForNextMove = false;
                StartCooldown();
                return;
            }

            movementTrait.SetDestination(sensedObjs[Random.Range(0, sensedObjs.Count)].position, OnMovementEnded);
            isMoving = true;
            readyForNextMove = false;
        }

        /// <summary>
        /// Start the cooldown timer.
        /// </summary>
        protected virtual void StartCooldown()
        {
            cooldownCoroutine = StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(OnCoolddownEnded, Cooldown));
        }

        /// <summary>
        /// Cooldown timer is up.
        /// </summary>
        protected virtual void OnCoolddownEnded()
        {
            readyForNextMove = true;
        }
        /// <summary>
        /// Movement ended due to reaching target or some other reason.
        /// </summary>
        /// <param name="successful">Successfully reached target?</param>
        protected virtual void OnMovementEnded(bool successful)
        {
            isMoving = false;
            StartCooldown();
        }

        protected virtual void OnValidate()
        {
            Cooldown = cooldown;
        }
    }
}
