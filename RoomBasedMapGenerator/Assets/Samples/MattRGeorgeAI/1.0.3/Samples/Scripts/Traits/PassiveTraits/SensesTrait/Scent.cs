﻿using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.AI.Samples.Traits
{
    /// <summary>
    /// This represents a smell.
    /// The Smell Trait looks for these objects.
    /// </summary>
    public class Scent : MonoBehaviour
    {
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;
        [Tooltip("The range value of this scent.")]
        [SerializeField] protected float range = 10;

        /// <summary>
        /// The range value of this scent.
        /// </summary>
        public float Range
        {
            get => range;
            set
            {
                range = (value > 0) ? value : 0;
            }
        }

        protected Coroutine currDespawnCoroutine = null;

        /// <summary>
        /// Start a despawn timer that automatically despawns this object.
        /// </summary>
        /// <param name="time">The time before this object is despawned.</param>
        public virtual void StartDespawnTimer(float time)
        {
            if (currDespawnCoroutine != null) StopCoroutine(currDespawnCoroutine);
            currDespawnCoroutine = StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Despawn, time));
        }

        /// <summary>
        /// Despawn this object.
        /// </summary>
        protected virtual void Despawn()
        {
            ObjectPoolManager.SINGLETON.Destroy(gameObject);
        }

        protected virtual void OnDrawGizmos()
        {
            if (isDebug)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, Range);
            }
        }
    }
}
