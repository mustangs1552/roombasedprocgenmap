﻿using MattRGeorge.Unity.Utilities.Components;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class KeySpawnNodes : MonoBehaviour
    {
        [Tooltip("The spawn objects to use for key spawns.")]
        [SerializeField] protected List<SpawnObject> nodes = new List<SpawnObject>();

        /// <summary>
        /// The spawn objects to use for key spawns.
        /// </summary>
        public List<SpawnObject> Nodes
        {
            get => nodes;
            set
            {
                if (value != null) nodes = value;
            }
        }
    }
}
