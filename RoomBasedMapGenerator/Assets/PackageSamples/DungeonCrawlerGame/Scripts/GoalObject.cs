﻿using MattRGeorge.Unity.Tools.Player;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class GoalObject : MonoBehaviour, IUsable
    {
        public bool Use(PlayerManager user)
        {
            ScoreController.SINGLETON.CurrScore++;
            ReloadScene();
            return true;
        }

        /// <summary>
        /// Reload the scene.
        /// </summary>
        public virtual void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
