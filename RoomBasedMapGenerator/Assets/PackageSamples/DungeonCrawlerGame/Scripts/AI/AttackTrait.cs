﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Tools.Stats;
using MattRGeorge.AI.AIIndividual;
using MattRGeorge.AI.Samples.Traits;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class AttackTrait : AITrait
    {
        [Tooltip("The base damage amount before any multipliers are applied.")]
        public Stat baseDamageStat = new Stat(25);
        [Tooltip("Multiplier applied to damage by number of room groups from the start room.")]
        public float roomGroupMultiplier = 1;
        [Tooltip("The range of the attack.")]
        [SerializeField] protected Stat rangeStat = new Stat(5);
        [Tooltip("The layer mask for the attack.")]
        public LayerMask explosionLayerMask = ~0;
        [Tooltip("Enable debug mode?")]
        public bool isDebug = false;
        public UnityEvent OnExplode = new UnityEvent();

        /// <summary>
        /// The damage with multipliers applied.
        /// </summary>
        public float DamageAmount => baseDamageStat.CurrAmount * (roomGroupCount * roomGroupMultiplier);

        /// <summary>
        /// The range of the attack.
        /// </summary>
        public float Range
        {
            get => rangeStat.CurrAmount;
            set
            {
                rangeStat.CurrAmount = (value < 0) ? 0 : value;
            }
        }

        /// <summary>
        /// The players to target.
        /// </summary>
        public List<Transform> Players
        {
            get => new List<Transform>(players);
            set
            {
                if (value == null) players = new List<Transform>();
                else players = ListUtility.RemoveNullUnityObjectEntries(value);
            }
        }

        protected SensesTrait sensesTrait = null;
        protected MovementTrait movementTrait = null;
        protected float roomGroupCount = -1;
        protected List<Transform> players = new List<Transform>();

        public override bool Checking()
        {
            return CheckForValidTarget();
        }

        public override bool Performing()
        {
            if (!movementTrait) return false;

            Transform target = CheckForValidTarget();
            if (!target) return false;

            if (CheckRangeFromTarget(target))
            {
                movementTrait.CancelDestination();
                Explode();
            }
            else movementTrait.SetDestination(target.position);
            
            return true;
        }

        public override void PreTraitSetup()
        {
            
        }
        public override void TraitSetup()
        {
            sensesTrait = Brain.GetTrait<SensesTrait>();
            if (!sensesTrait) UnityLoggingUtility.LogMissingValue(GetType(), "sensesTrait", gameObject);

            movementTrait = Brain.GetTrait<MovementTrait>();
            if (!movementTrait) UnityLoggingUtility.LogMissingValue(GetType(), "movementTrait", gameObject);
        }
        public override void PostTraitSetup()
        {
            FindValidPlayers();
            ResetStats();

            roomGroupCount = Brain.GetComponent<RBMGAIBrain>().GetRoomGroupsDepth();
        }

        public override void TraitReset()
        {
            FindValidPlayers();
            ResetStats();
        }

        public override bool CancelPerform()
        {
            if (!movementTrait) return true;

            movementTrait.CancelDestination();
            return true;
        }

        /// <summary>
        /// Reset the stat values to their starting amounts.
        /// </summary>
        public virtual void ResetStats()
        {
            baseDamageStat.StatReset();
            rangeStat.StatReset();
        }

        /// <summary>
        /// Find all the valid players that should be targeted.
        /// </summary>
        protected virtual void FindValidPlayers()
        {
            GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");
            List<Transform> playerTrans = new List<Transform>();
            foreach (GameObject obj in playerObjs) playerTrans.Add(obj.transform);
            Players = playerTrans;
        }

        /// <summary>
        /// Check for a valid target to target.
        /// </summary>
        /// <returns>The valid target if one was found.</returns>
        protected virtual Transform CheckForValidTarget()
        {
            if (!sensesTrait || Players.Count == 0) return null;

            List<Transform> sensedPlayers = sensesTrait.CheckSenses(Players);
            return (sensedPlayers != null && sensedPlayers.Count > 0) ? sensedPlayers[0] : null;
        }

        /// <summary>
        /// Check if the target was reached.
        /// </summary>
        /// <param name="target">The target to check.</param>
        /// <returns>True if reached.</returns>
        protected virtual bool CheckRangeFromTarget(Transform target)
        {
            if (!target) return false;

            return Vector3.Distance(transform.position, target.position) <= Range;
        }
        /// <summary>
        /// Explode immediatly, dealing damage to everything within the range.
        /// </summary>
        protected virtual void Explode()
        {
            Collider[] hitObjs = Physics.OverlapSphere(transform.position, Range, explosionLayerMask);
            Health healthObj = null;
            foreach(Collider obj in hitObjs)
            {
                healthObj = obj.GetComponent<Health>();
                if (!healthObj) continue;

                healthObj.TakeDamage(DamageAmount);
            }

            OnExplode?.Invoke();
            ObjectPoolManager.SINGLETON.Destroy(Brain.gameObject);
        }

        protected void OnDrawGizmos()
        {
            if (!isDebug) return;

            Transform playerSensed = CheckForValidTarget();
            foreach(Transform player in Players)
            {
                if (player == playerSensed) Gizmos.color = Color.green;
                else Gizmos.color = Color.red;

                Gizmos.DrawLine(transform.position, player.position);
            }
        }
    }
}
