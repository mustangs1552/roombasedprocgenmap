﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Objects;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class KeySpawner : MonoBehaviour
    {
        public static KeySpawner SINGLETON = null;

        [Tooltip("The room groups generator to spawn keys for.")]
        [SerializeField] protected RandomRoomGroupsGenerator groupsGenerator = null;
        [Tooltip("The map's starting room.")]
        [SerializeField] protected Room startRoom = null;
        [Tooltip("The object spawner use to spawn the keys.")]
        [SerializeField] protected ObjectSpawner spawner = null;
        [Tooltip("The key object to spawn.")]
        [SerializeField] protected UsableKey key = null;
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;

        /// <summary>
        /// Spawns a key for every generator that the assigned group generator has spawned.
        /// </summary>
        /// <param name="includeStartRoom">Spawn a key for the starting room entrance?</param>
        public virtual void SpawnAllZoneKeys(bool includeStartRoom)
        {
            if (!groupsGenerator || !startRoom || !spawner || !key) return;

            List<RandomRoomsGenerator> generators = groupsGenerator.Generators;
            foreach(RandomRoomsGenerator generator in generators)
            {
                if (!generator.StartingEntrance || !generator.StartingEntrance.Room || (!includeStartRoom && generator.StartingEntrance.Room == startRoom)) continue;

                SpawnKey(generator.StartingEntrance);
            }
        }

        /// <summary>
        /// Spawn a key for the entrance object that was just spawned if it is one that requires a key.
        /// </summary>
        /// <param name="entrance">The entrance object that was spawned.</param>
        public virtual void SpawnKey(Entrance entrance, Key customKey = null)
        {
            if (!spawner || !entrance || !entrance.Room) return;
            if (!customKey) customKey = key;
            if (!customKey) return;

            List<Room> validRooms = FindValidRooms(entrance);
            if (validRooms.Count == 0) return;

            List<SpawnObject> spawnNodes = FindKeySpawnObjects(validRooms);
            spawner.SpawnObjects(spawnNodes, customKey.gameObject, 1);
            if (isDebug) Debug.Log(entrance.gameObject.name + " | " + entrance.Room.gameObject.name + " | " + entrance.Room.transform.parent.parent.name);
        }

        /// <summary>
        /// Find all the key spawn objects in the given rooms.
        /// </summary>
        /// <param name="rooms">The rooms to look through.</param>
        /// <returns>The list of spawn objects.</returns>
        protected virtual List<SpawnObject> FindKeySpawnObjects(List<Room> rooms)
        {
            if (rooms == null || rooms.Count == 0) return new List<SpawnObject>();

            List<SpawnObject> spawnNodes = new List<SpawnObject>();
            KeySpawnNodes currKeySpawnNodeList = null;
            foreach (Room room in rooms)
            {
                currKeySpawnNodeList = room.GetComponent<KeySpawnNodes>();
                if (!currKeySpawnNodeList) continue;

                currKeySpawnNodeList.Nodes.ForEach(x => spawnNodes.Add(x));
            }

            return spawnNodes;
        }
        /// <summary>
        /// Find all the valid rooms that can have a key spawned in for the given entrance.
        /// </summary>
        /// <param name="entrance">The entrance that needs a key spawned.</param>
        /// <returns>The valid rooms for a key spawn.</returns>
        protected virtual List<Room> FindValidRooms(Entrance entrance)
        {
            if (!startRoom || !entrance || !entrance.Room || !entrance.OtherEntrance || !entrance.OtherEntrance.Room) return new List<Room>();

            List<Room> entrancePath = RBMGDistanceUtility.FindAStarPathByRoom(startRoom, entrance.Room);
            List<Room> otherEntrancePath = RBMGDistanceUtility.FindAStarPathByRoom(startRoom, entrance.OtherEntrance.Room);
            List<Room> closestPath = (entrancePath.Count < otherEntrancePath.Count) ? entrancePath : otherEntrancePath;
            List<RandomRoomsGenerator> generators = new List<RandomRoomsGenerator>();
            foreach (Room room in closestPath)
            {
                if (room.Generator && !generators.Contains(room.Generator)) generators.Add(room.Generator);
            }
            List<Room> validRooms = new List<Room>();
            generators.ForEach(x => x.GeneratedRooms.ForEach(y => validRooms.Add(y)));
            return validRooms;
        }

        /// <summary>
        /// Check for missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!groupsGenerator) UnityLoggingUtility.LogMissingValue(GetType(), "roomGroupsGenerator", gameObject);
            if (!startRoom) UnityLoggingUtility.LogMissingValue(GetType(), "startRoom", gameObject);
            if (!spawner) UnityLoggingUtility.LogMissingValue(GetType(), "spawner", gameObject);
            if (!key) UnityLoggingUtility.LogMissingValue(GetType(), "key", gameObject);
        }

        protected virtual void Awake()
        {
            CheckMissingValues();

            if (!KeySpawner.SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError("More than one 'KeySpawner' singletons detected! Removing second...");
                Destroy(this);
            }
        }
    }
}
