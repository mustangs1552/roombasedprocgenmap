﻿using UnityEngine;
using MattRGeorge.Unity.Objects;
using MattRGeorge.Unity.Tools.InventorySystem;
using MattRGeorge.Unity.Tools.Player;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class UsableKey : Key
    {
        [Tooltip("The name of the inventory on the player to use to store collected keys.")]
        [SerializeField] protected string keyInventory = "MainInventory";

        /// <summary>
        /// Add this key to the user's inventory.
        /// </summary>
        /// <param name="user">The user using this key.</param>
        /// <returns>True if successful.</returns>
        protected override bool PerformUse(PlayerManager user)
        {
            if (!user) return false;
            GameObject userInventoryObj = user.GetObject(keyInventory);
            if (!userInventoryObj) return false;
            Inventory userInventory = userInventoryObj.GetComponent<Inventory>();
            if (!userInventory) return false;

            userInventory.AddItem(this);
            gameObject.SetActive(false);
            return true;
        }
    }
}
