﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Objects;
using MattRGeorge.Unity.Tools.Player;
using MattRGeorge.Unity.Tools.InventorySystem;

namespace MattRGeorge.RoomBasedMapGenerator.DungeonCrawlerGame
{
    public class UsableDoor : Door, IUsable
    {
        [Tooltip("The name of the inventory on the user that stores their collected keys.")]
        [SerializeField] protected string keyInventory = "MainInventory";

        /// <summary>
        /// Open/close this door when the user uses unlocking it first if the user has the required key.
        /// </summary>
        /// <param name="user">The user using this door.</param>
        /// <returns>True when successful.</returns>
        public virtual bool Use(PlayerManager user)
        {
            if(!IsOpen)
            {
                if (lockObj.IsLocked) return AttemptUnlock(user);
                else
                {
                    Open();
                    return true;
                }
            }
            else
            {
                Close();
                return true;
            }
        }

        /// <summary>
        /// Attempt to unlock this door for the given user.
        /// </summary>
        /// <param name="user">The user trying to unlock this door.</param>
        /// <returns>True is successful.</returns>
        public virtual bool AttemptUnlock(PlayerManager user)
        {
            if (!user) return false;
            GameObject userInventoryObj = user.GetObject(keyInventory);
            if (!userInventoryObj) return false;
            Inventory userInventory = userInventoryObj.GetComponent<Inventory>();
            if (!userInventory) return false;

            List<Key> keys = GetUserKeys(userInventory);
            foreach (Key key in keys)
            {
                if (Unlock(key))
                {
                    userInventory.RemoveItem(key);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get all the keys that the user has stored.
        /// </summary>
        /// <param name="userInventory">The user's inventory with their stored keys.</param>
        /// <returns>The list of keys.</returns>
        protected virtual List<Key> GetUserKeys(Inventory userInventory)
        {
            if (!userInventory) return new List<Key>();

            List<Key> keys = new List<Key>();
            Key currKey = null;
            foreach (KeyValuePair<string, List<InventorySlot>> slots in userInventory.Slots)
            {
                if (slots.Value.Count == 0) continue;

                currKey = slots.Value[0].StoredItem.GetComponent<Key>();
                if (currKey)
                {
                    foreach (InventorySlot slot in slots.Value) slot.StoredItems.ForEach(x => keys.Add(x.GetComponent<Key>()));
                }
            }

            return keys;
        }
    }
}
